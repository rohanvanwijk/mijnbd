//
//  HistoryNavigationController.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 16/11/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit

class HistoryNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        // Add history table view
        let historyTable: UITableViewController = HistoryTableViewController()
        let tableview: UITableView = UITableView()
        tableview.dataSource = historyTable
        tableview.delegate = historyTable
        self.view.addSubview(tableview)
        self.viewControllers = [historyTable]
    }
}
