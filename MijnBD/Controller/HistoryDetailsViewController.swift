//
//  HistoryDetailsViewController.swift
//  MijnBD
//
//  Created by Kevin Sweijen on 20/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class HistoryDetailsViewController : UIViewController {
    public var model: HistoryModel!
    
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelOrderId: UILabel!
    @IBOutlet weak var imgVehicle: UIImageView!
    
    @IBOutlet weak var labelWarehouseAdres: UILabel!
    @IBOutlet weak var labelWarehouseTime: UILabel!
    @IBOutlet weak var labelWarehouseDistance: UILabel!
    
    @IBOutlet weak var labelCustomerAdres: UILabel!
    @IBOutlet weak var labelCustomerTime: UILabel!
    @IBOutlet weak var labelCustomerDistance: UILabel!
    
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelTip: UILabel!
    @IBOutlet weak var labelPriceTotal: UILabel!
    
    @IBOutlet weak var labelReady: UILabel!
    @IBOutlet weak var labelAccepted: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
    }
    
    private func initSetup() {
        self.navigationController?.navigationBar.tintColor = .black
        
        // Setting labels
        setLabelStatus()
        setLabelDate()
        setVehicleIcon()
        setPrice()
        
        labelOrderId.text = ""
        labelWarehouseAdres.text = model.warehouse.address
        labelCustomerAdres.text = model.customer.address
        labelWarehouseDistance.text = "\(String(format: "%.1f", model.warehouseDistanceInKilometers)) km. \(NSLocalizedString("history_distance", comment: ""))"
        labelCustomerDistance.text = "\(String(format: "%.1f", model.customerDistanceInKilometers)) km. \(NSLocalizedString("history_distance", comment: ""))"
        labelWarehouseTime.text = "\(getTime(input: model.warehousePickUpAt, format: "HH:mm")) \(NSLocalizedString("history_order_pickedup", comment: ""))"
        labelCustomerTime.text = "\(getTime(input: model.deliveredAt, format: "HH:mm")) \(NSLocalizedString("history_order_delivered", comment: ""))"
        labelReady.text = "\(getTime(input: model.createdAt!, format: "HH:mm")) Gereed magazijn"
        labelAccepted.text = "\(getTime(input: model.acceptedAt!, format: "HH:mm")) Geaccepteerd"
    }
    
    private func setLabelStatus() {
        var msg = NSLocalizedString("order_success", comment: "")
        var textColor = UIColor.systemGreen
        if model.status != 4 {
            msg = NSLocalizedString("order_unsuccess", comment: "")
            textColor = UIColor.systemRed
        }
        labelStatus.textColor = textColor
        labelStatus.text = msg
    }
    
    private func setLabelDate() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSSSSS"
        if model.deliveredAt.contains("+") {
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSSSSS'+01:00'"
        }
        let date_d = dateFormatter.date(from: model.deliveredAt)!
        
        let stringFormatter = DateFormatter()
        stringFormatter.dateFormat = "HH:mm dd-MM-yyyy"
        let time = stringFormatter.string(from: date_d)
        labelDate.text = time
    }
    
    private func getTime(input: String, format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSSSSS"
        if input.contains("+") {
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSSSSS'+01:00'"
        }
        let date_d = dateFormatter.date(from: input)!
        
        let stringFormatter = DateFormatter()
        stringFormatter.dateFormat = format
        let time = stringFormatter.string(from: date_d)
        return time
    }
    
    private func setVehicleIcon() {
        var imgName = "ic_car_white"
        if model.vehicle == 1 {
            imgName = "ic_bike_white"
        } else if model!.vehicle == 2 || model!.vehicle == 3 {
            imgName = "ic_motor_white"
        }
        let image = UIImage(named: imgName)
        imgVehicle.image = image
    }
    
    private func setPrice() {
        let price = model.price
        var tip = model.tip
        if tip == nil {
            tip = 0
        }
        let totalPrice = price + tip!
        
        labelPrice.text = "€ \(String(format: "%.2f", price)) \(NSLocalizedString("order_delivery", comment: "")) \(mapVehicle(_vehicle: model.vehicle))"
        labelTip.text = "€ \(String(format: "%.2f", tip!)) \(NSLocalizedString("order_tip", comment: ""))"
        labelPriceTotal.text = "€ \(String(format: "%.2f", totalPrice)) \(NSLocalizedString("order_total", comment: ""))"
    }
    
    private func mapVehicle(_vehicle: Int) -> String {
        switch _vehicle {
        case 1:
            return "Fiets"
        case 2:
            return "Scooter"
        case 3:
            return "Motor"
        default:
            return "Auto"
        }
    }
}
