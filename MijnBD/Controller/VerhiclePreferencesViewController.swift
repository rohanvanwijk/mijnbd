//
//  VerhiclePreferencesViewController.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 08/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit
import SwiftKeychainWrapper

class VerhiclePreferencesViewController: UIViewController {
    
    var vehicle = 1
    var vehicleDisplayName = ""
    
    @IBOutlet weak var radiusLabel: UILabel!
    @IBOutlet weak var radiusSlider: UISlider!
    @IBOutlet weak var bicycleButton: UIButton!
    @IBOutlet weak var motorScooterButton: UIButton!
    @IBOutlet weak var carButton: UIButton!
    @IBOutlet weak var meansOfTransportLabel: UILabel!
    @IBOutlet weak var radiusTitleLabel: UILabel!
    @IBOutlet weak var minimumRadiusLabel: UILabel!
    @IBOutlet weak var maximumRadiusLabel: UILabel!
    @IBOutlet weak var radiusFromHomeLabel: UILabel!
    @IBOutlet weak var radiusFrame: UIView!
    @IBOutlet weak var labelEmpty: UILabel!
    
    
    private var bezorger: DelivererModel?
    
    @IBAction func bicycleButton(_ sender: Any) {
        bicycleButton.backgroundColor = UIColor.systemYellow
        bicycleButton.setTitleColor(UIColor.black, for: .normal)
        motorScooterButton.backgroundColor = UIColor.darkGray
        motorScooterButton.setTitleColor(UIColor.white, for: .normal)
        carButton.backgroundColor = UIColor.darkGray
        carButton.setTitleColor(UIColor.white, for: .normal)
        vehicle = 1
        vehicleDisplayName = "Fiets"
    }
    @IBAction func motorScooterButton(_ sender: Any) {
        bicycleButton.backgroundColor = UIColor.darkGray
        bicycleButton.setTitleColor(UIColor.white, for: .normal)
        motorScooterButton.backgroundColor = UIColor.systemYellow
        motorScooterButton.setTitleColor(UIColor.black, for: .normal)
        carButton.backgroundColor = UIColor.darkGray
        carButton.setTitleColor(UIColor.white, for: .normal)
        vehicle = 2
        vehicleDisplayName = "Motor/scooter"
    }
    @IBAction func carButton(_ sender: Any) {
        bicycleButton.backgroundColor = UIColor.darkGray
        bicycleButton.setTitleColor(UIColor.white, for: .normal)
        motorScooterButton.backgroundColor = UIColor.darkGray
        motorScooterButton.setTitleColor(UIColor.white, for: .normal)
        carButton.backgroundColor = UIColor.systemYellow
        carButton.setTitleColor(UIColor.black, for: .normal)
        vehicle = 4
        vehicleDisplayName = "Auto"
    }
    
    @IBAction func radiusSlider(_ sender: Any) {
        let currentValue = Int(radiusSlider.value)
        radiusLabel.text = "\(currentValue) Km"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getPerson()
        setLabels()
    }
    
    private func initSetup() {
        let saveButton = UIBarButtonItem(title: NSLocalizedString("userinfo_save_button_text", comment: ""), style: .plain, target: self, action: #selector(saveChangesButton(_:)))
        saveButton.tintColor = .black
        self.navigationItem.rightBarButtonItem = saveButton
        
        // Styling
        self.navigationItem.title = NSLocalizedString("verhicleprefs_title", comment: "")
        self.navigationController?.navigationBar.barTintColor = .systemYellow
        self.navigationController?.navigationBar.tintColor = .black
        self.view.backgroundColor = .black
        bicycleButton.layer.masksToBounds = true
        bicycleButton.layer.cornerRadius = 10
        bicycleButton.layer.borderWidth = 2
        bicycleButton.layer.borderColor = UIColor.black.cgColor
        motorScooterButton.layer.masksToBounds = true
        motorScooterButton.layer.cornerRadius = 10
        motorScooterButton.layer.borderWidth = 2
        motorScooterButton.layer.borderColor = UIColor.black.cgColor
        carButton.layer.masksToBounds = true
        carButton.layer.cornerRadius = 10
        carButton.layer.borderWidth = 2
        carButton.layer.borderColor = UIColor.black.cgColor
        radiusFrame.layer.cornerRadius = 10
        labelEmpty.text = ""
        
        //set
        meansOfTransportLabel.text = NSLocalizedString("verhicleprefs_means_of_transport_label", comment: "")
        bicycleButton.setTitle(NSLocalizedString("verhicleprefs_bicycle_button_text", comment: ""), for: .normal)
        motorScooterButton.setTitle(NSLocalizedString("verhicleprefs_motor_scooter_button_text", comment: ""), for: .normal)
        carButton.setTitle(NSLocalizedString("verhicleprefs_car_button_text", comment: ""), for: .normal)
        radiusTitleLabel.text = NSLocalizedString("verhicleprefs_radius_title_label", comment: "")
        minimumRadiusLabel.text = NSLocalizedString("verhicleprefs_minimum_radius_label", comment: "")
        maximumRadiusLabel.text = NSLocalizedString("verhicleprefs_maximum_radius_label", comment: "")
        radiusFromHomeLabel.text = NSLocalizedString("verhicleprefs_radius_from_home_label", comment: "")
    }
    
    private func setLabels() {
        guard let p = bezorger else { return }
        
        radiusSlider.setValue(Float(p.range), animated: false)
        radiusLabel.text = "\(String(p.range)) Km"
        
        // actions
        vehicle = p.vehicle
        if (vehicle == 1 ){bicycleButton.sendActions(for: .touchUpInside)}
        else if (vehicle == 2 || vehicle == 3){motorScooterButton.sendActions(for: .touchUpInside)}
        else if (vehicle == 4){carButton.sendActions(for: .touchUpInside)}
        else {print("Error: Vehicle 0")}
        
        // Set active buttons
        if p.vehicle == 1 {
            bicycleButton.backgroundColor = .systemYellow
        } else if p.vehicle == 2 || p.vehicle == 3 {
            motorScooterButton.backgroundColor = .systemYellow
        } else {
            carButton.backgroundColor = .systemYellow
        }
    }
    
    private func getPerson() {
        guard let data = KeychainWrapper.standard.data(forKey: "person") else { return }
        let decoder = JSONDecoder()
        do {
            let deliverer  = try
            decoder.decode(DelivererModel.self, from: data)
            self.bezorger = deliverer
        } catch let error {
            print(error)
        }
    }
    
    @IBAction func saveChangesButton(_ sender: Any) {
        save()
    }
    
    private func save() {
        guard let p = bezorger else { return }
        let range = Int(radiusSlider.value)
        
        let updatedPerson = DelivererModel(phoneNumber: p.phoneNumber, homeID: p.homeID, dateOfBirth: p.dateOfBirth, range: range, vehicle: self.vehicle, fare: p.fare, totalEarnings: p.totalEarnings, home: p.home, id: p.id, firstName: p.firstName, lastName: p.lastName, emailAddress: p.emailAddress, token: p.token)
        
        ServiceManager.updateDeliverer(person: updatedPerson, token: updatedPerson.token).responseData(completionHandler: { response in
            guard let responseData = response.data else { return }
            guard let httpcode = response.response?.statusCode else { return }
            
            print(httpcode)
            if httpcode == 200 {
                KeychainWrapper.standard.set(responseData, forKey: "person")
                self.navigationController?.popViewController(animated: true)
            }
            
        })
    }
}
