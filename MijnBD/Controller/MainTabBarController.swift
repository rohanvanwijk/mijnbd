//
//  MainTabBarController.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 16/11/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit
import SwiftKeychainWrapper

class MainTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLogin()
    }
    
    private func initSetup() {
        // Navigation controllers
        let historyNavController = HistoryNavigationController()
        let deliveryNavController = UINavigationController()
        let settingsNavController = UINavigationController()
        
        // Assign view controllers to navigation controllers
        let deliveryViewController = DeliveryViewController()
        let settingsViewController = SettingsViewController()
        deliveryNavController.viewControllers = [deliveryViewController]
        settingsNavController.viewControllers = [settingsViewController]
        
        historyNavController.tabBarItem = UITabBarItem(title: NSLocalizedString("history_title", comment: ""), image: UIImage(named: "iconClipboard"), tag: 0)
        deliveryNavController.tabBarItem = UITabBarItem(title: NSLocalizedString("delivery_title", comment: ""), image: UIImage(named: "iconClock"), tag: 1)
        settingsNavController.tabBarItem = UITabBarItem(title: NSLocalizedString("settings_title", comment: ""), image: UIImage(named: "iconUser"), tag: 2)
        
        self.tabBar.tintColor = .systemYellow
        self.tabBar.unselectedItemTintColor = .black
        
        let tabBarList = [historyNavController, deliveryNavController, settingsNavController]
        viewControllers = tabBarList
        
        // Start at delivery
        self.selectedIndex = 1
    }
    
    private func checkLogin() {
        var loggedIn = false
        if let KeychainToken = KeychainWrapper.standard.string(forKey: "token") {
            if KeychainToken != "" {
                loggedIn = true
            }
        }
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC: UIViewController = storyboard.instantiateViewController(withIdentifier: "loginVC") as UIViewController
        
        if(!loggedIn) {
            loginVC.modalPresentationStyle = .fullScreen
            self.present(loginVC, animated: true, completion: nil)
        }
    }
}
