//
//  AddAvailabilityViewController.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 24/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit
import SwiftKeychainWrapper

class AddAvailabilityViewController: UIViewController {
    var list: [AvailabilityModel] = []
    
    // Outlets
    @IBOutlet weak var inputStartDate: UITextField!
    @IBOutlet weak var inputEndDate: UITextField!
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var labelStart: UILabel!
    @IBOutlet weak var labelEnd: UILabel!
    
    private var datepicker: UIDatePicker?
    private var endDatepicker: UIDatePicker?
    public var date: AvailabilityModel?
    private var startDate: String?
    private var startDate2: Date?
    public var isPut = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
    }
    
    private func initSetup() {
        setupDatePicker()
        
        datepicker?.addTarget(self, action: #selector(startDateChanged(datepicker:)), for: .valueChanged)
        endDatepicker?.addTarget(self, action: #selector(endDateChanged(datepicker:)), for: .valueChanged)
        buttonSubmit.addTarget(self, action: #selector(submit), for: .touchUpInside)
        
        buttonSubmit.setTitle(NSLocalizedString("cancel_delivery_submit", comment: ""), for: .normal)
        labelStart.text = NSLocalizedString("a_start", comment: "")
        labelEnd.text = NSLocalizedString("a_end", comment: "")
        
        setInputLabel()
        getAvailabilities()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func tapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    private func setInputLabel() {
        if isPut {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss"
            let date_d = dateFormatter.date(from: date!.date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let date_s = dateFormatter.string(from: date_d!)
            
            let timeFormatter = DateFormatter()
            timeFormatter.dateFormat = "HH':'mm':'ss"
            let startTime_d = timeFormatter.date(from: date!.startTime)
            let endTime_d = timeFormatter.date(from: date!.endTime)
            timeFormatter.dateFormat = "HH:mm"
            let startTime_s = timeFormatter.string(from: startTime_d!)
            let endTime_s = timeFormatter.string(from: endTime_d!)
            
            inputStartDate.text = "\(date_s) \(startTime_s)"
            inputEndDate.text = "\(date_s) \(endTime_s)"
        }
    }
    
    @objc func startDateChanged(datepicker: UIDatePicker) {
        let inputDateFormatter = DateFormatter()
        let modelDateFormatter = DateFormatter()
        let usDateFormatter = DateFormatter()
        let modelStartTimeFormatter = DateFormatter()
        modelDateFormatter.dateFormat = "dd-MM-yyyy"
        usDateFormatter.dateFormat = "yyyy-MM-dd"
        modelStartTimeFormatter.dateFormat = "HH:mm"
        inputDateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        startDate2 = datepicker.date
        
        let inputDate = modelDateFormatter.string(from: datepicker.date)
        let inputStartTime = modelStartTimeFormatter.string(from: datepicker.date)
        let usInputDate = usDateFormatter.string(from: datepicker.date)
        startDate = inputDate
        
        if date == nil {
            date = AvailabilityModel(id: "id", delivererID: "delid", date: usInputDate, startTime: inputStartTime, endTime: "")
        } else {
            date!.date = usInputDate
            date!.startTime = inputStartTime
        }
        
        inputStartDate.text = inputDateFormatter.string(from: datepicker.date)
    }
    
    @objc func endDateChanged(datepicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        let modelEndTimeFormatter = DateFormatter()
        modelEndTimeFormatter.dateFormat = "HH:mm"
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        
        let inputendTime = modelEndTimeFormatter.string(from: datepicker.date)
        
        if date == nil {
            date = AvailabilityModel(id: "id", delivererID: "delid", date: "", startTime: "", endTime: inputendTime)
        } else {
            date!.endTime = inputendTime
        }
        
        if startDate != nil {
            inputEndDate.text = "\(startDate!) \(inputendTime)"
        } else {
            inputEndDate.text = inputendTime
        }
    }
    
    @objc func submit() {
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        guard let amodel = date else { return }
        if !inputIsValide(model: amodel) {
            alert(title: NSLocalizedString("availability_title", comment: ""), message: NSLocalizedString("a_input_empty", comment: ""))
        } else {
            if compareForValidation(model: amodel){
                if isPut {
                    ServiceManager.editAvailability(token: token, model: amodel).responseData(completionHandler: { response in
                        guard let httpcode = response.response?.statusCode else { return }
                        if httpcode == 200 {
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            self.alert(title: NSLocalizedString("connection_error_title", comment: ""), message: "Error: \(httpcode)")
                        }
                    })
                } else {
                    ServiceManager.addAvailability(token: token, availability: amodel).responseData(completionHandler: { response in
                        guard let httpcode = response.response?.statusCode else { return }
                        if httpcode == 200 {
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            self.alert(title: NSLocalizedString("connection_error_title", comment: ""), message: "Error: \(httpcode)")
                        }
                    })
                }
            }
        }
    }
    
    private func alert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: "ok"), style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func inputIsValide(model: AvailabilityModel) -> Bool {
        if model.endTime != "" && model.startTime != "" && model.endTime != "" {
            return true
        }
        return false
    }
    
    private func setupDatePicker() {
        var dateComponent = DateComponents()
        dateComponent.month = 2
        let today = Date()
        
        datepicker = UIDatePicker()
        endDatepicker = UIDatePicker()
        datepicker?.datePickerMode = .dateAndTime
        datepicker?.minimumDate = today
        endDatepicker?.datePickerMode = .time
        datepicker?.minuteInterval = 5
        endDatepicker?.minuteInterval = 5
        inputStartDate.inputView = datepicker
        inputEndDate.inputView = endDatepicker
    }
    
    private func getAvailabilities(){
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        ServiceManager.getAvailabilities(token: token).responseJSON(completionHandler: { response in
            guard let jsonData = response.data else {return}
            let decoder = JSONDecoder()
            do {
                let result = try
                    decoder.decode([AvailabilityModel].self, from: jsonData)
                self.list = result
            } catch let error {
                print(error)
            }
        })
    }
    
    private func compareForValidation(model: AvailabilityModel) -> Bool {
        for (index, _) in list.enumerated() {
                   
            let currentDate = Date()
            var dateComponent = DateComponents()
            dateComponent.month = 2
            let futureDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
                   
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd"
            
            let futureDateString = format.string(from: futureDate!)
            
            var dateSequenceChecker = [model.date, futureDateString]
            dateSequenceChecker.sort(by: {$0 < $1})
            if (dateSequenceChecker[0] != model.date){
                alert(title: NSLocalizedString("date_out_of_bounds_title", comment: ""), message: NSLocalizedString("date_out_of_bounds_message", comment: ""))
                return false
            }
            
            var pass1 = false
            var pass2 = false

            let availabilityArray = list[index].date.components(separatedBy: "T")
            
            var timeSequenceCheckerList: [String] = [String(list[index].startTime.dropLast(3)), String(list[index].endTime.dropLast(3)), model.startTime, model.endTime]
            if (String(list[index].startTime.dropLast(3)) == model.startTime || String(list[index].endTime.dropLast(3)) == model.endTime){
                alert(title: NSLocalizedString("invalid_date_error_title", comment: ""), message: NSLocalizedString("invalid_date_error_message", comment: "") )
                return false
            }
            timeSequenceCheckerList.sort(by: {$0 < $1})
            
            if (model.date == availabilityArray[0]){
                for (index2, _) in timeSequenceCheckerList.enumerated(){
                        if (timeSequenceCheckerList[index2] == String(list[index].startTime.dropLast(3))){
                            if (index2+1 < timeSequenceCheckerList.count && timeSequenceCheckerList[index2+1] == String(list[index].endTime.dropLast(3))){
                                pass1 = true
                            }
                        }
                        if (timeSequenceCheckerList[index2] == model.startTime){
                            if (index2+1 < timeSequenceCheckerList.count && timeSequenceCheckerList[index2+1] == model.endTime) {
                                pass2 = true
                            }
                        }
                }
                if (!(pass1 && pass2)){
                    alert(title: NSLocalizedString("invalid_date_error_title", comment: ""), message: NSLocalizedString("invalid_date_error_message", comment: "") )
                    return false
                }
            }
            
        }
        getAvailabilities()
        return true
    }
}
