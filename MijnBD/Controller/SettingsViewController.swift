//
//  SettingsViewController.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 17/11/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import SwiftKeychainWrapper

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        return sv
        
    }()
    let contentView: UIView = {
        let v = UIView()
        return v
    }()
    let profilePicture: UIImageView = {
        let imageView = UIImageView()
        let profileImage = UIImage(named: "avatar")
        imageView.image = profileImage
        imageView.layer.cornerRadius = 8
        imageView.clipsToBounds = true
        return imageView
    }()
    let verhicleTypeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = NSLocalizedString("verhicleType", comment: "")
        return label
    }()
    let fareLabel: UILabel = {
        let label = UILabel()
        label.text = NSLocalizedString("fare", comment: "")
        label.textColor = .white
        return label
    }()
    let totalEarningsLabel: UILabel = {
        let label = UILabel()
        label.text = NSLocalizedString("totalEarnings", comment: "")
        label.textColor = .white
        return label
    }()
    let personName: UILabel = {
        let personName = UILabel()
        personName.textColor = .white
        personName.font = UIFont.boldSystemFont(ofSize: 27)
        return personName
    }()
    let userViewContainer: UIView = {
        let userView = UIView()
        userView.backgroundColor = .darkGray
        userView.layer.cornerRadius = 20
        return userView
    }()
    let userView: UIView = {
        let view = UIView()
        return view
    }()
    let personVerhicleType: UILabel = {
        let label = UILabel()
        label.textColor = .systemYellow
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        return label
    }()
    let personFare: UILabel = {
        let label = UILabel()
        label.textColor = .systemYellow
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        return label
    }()
    let personTotalEarnings: UILabel = {
        let label = UILabel()
        label.textColor = .systemYellow
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        return label
    }()
    let buttonsView: UIView = {
        let view = UIView()
        return view
    }()
    let infoButton: UIButton = {
        let button = UIButton()
        button.setTitle(NSLocalizedString("info_button_title", comment: ""), for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.layer.cornerRadius = 10
        button.backgroundColor = .systemYellow
        return button
    }()
    let verhicleButton: UIButton = {
         let button = UIButton()
        button.setTitle(NSLocalizedString("verhicleprefs_button_title", comment: ""), for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.layer.cornerRadius = 10
        button.backgroundColor = .systemYellow
        return button
    }()
    let availabilityButton: UIButton = {
         let button = UIButton()
        button.setTitle(NSLocalizedString("availability_button_title", comment: ""), for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.layer.cornerRadius = 10
        button.backgroundColor = .systemYellow
        return button
    }()
    let availabilityView: UIView = {
        let view = UIView()
        view.backgroundColor = .darkGray
        view.layer.cornerRadius = 20
        return view
    }()
    let availabilityLabel: UILabel = {
        let label = UILabel()
        label.text = NSLocalizedString("availability_table_title", comment: "")
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 27)
        return label
    }()
    let availabilityBottomLabel: UILabel = {
        let label = UILabel()
        label.text = " "
        label.font = UIFont.boldSystemFont(ofSize: 27)
        return label
    }()
    var availabilityTableView : UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .darkGray
        return tableView
    }()
    
    struct avDay {
        let day : String
        let startTime: String
        let endTime: String
    }
    
    let weekDays : [String] = [NSLocalizedString("sunday", comment: ""),
                               NSLocalizedString("monday", comment: ""),
                               NSLocalizedString("tuesday", comment: ""),
                               NSLocalizedString("wednesday", comment: ""),
                               NSLocalizedString("thursday", comment: ""),
                               NSLocalizedString("friday", comment: ""),
                               NSLocalizedString("saturday", comment: "")]
    
    var deliverer: DelivererModel = DelivererModel(phoneNumber: "0612345678", homeID: "1c0fd247df77", dateOfBirth: "1997-01-18T00:00:00", range: 12, vehicle: 1, fare: 12.0, totalEarnings: nil, home: Home(id: "id", latitude: 1.11, longitude: 1.11, address: "adr", postalCode: "22", place: "ams", isWarehouse: false), id: "ss", firstName: "name", lastName: "name", emailAddress: "person@mail.com", token: "token")
    
    var list: [AvailabilityModel] = []
    var avList: [avDay] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
        setUserImage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getPerson()
        getAvailabilityData()
        setUserImage()
    }
    
    private func initSetup() {
        // Styling
        self.navigationItem.title = NSLocalizedString("settings_title", comment: "")
        self.navigationController?.navigationBar.barTintColor = .systemYellow
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        
        let logoutButton = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(logout))
        logoutButton.tintColor = .black
        self.navigationItem.rightBarButtonItem = logoutButton
        
        //availabilityTableView.frame = availabilityView.frame
        let cellNib = UINib(nibName: "SettingsAvailabilityCell", bundle: nil)
        availabilityTableView.register(cellNib, forCellReuseIdentifier: "avCell")
        availabilityTableView.delegate = self
        availabilityTableView.dataSource = self
        setupConstraints()
    }
    
    @objc func logout() {
        // 1. call to backend
        guard let token = KeychainWrapper.standard.string(forKey: "token") else {return}
        ServiceManager.logout(token: token).responseJSON(completionHandler: { response in
            guard let httpCode = response.response?.statusCode else { return }
            
            if httpCode == 200 {
                // 2. remove all keychain objects
                KeychainWrapper.standard.removeAllKeys()
                print("Logged out: \(self.deliverer.emailAddress)")
                // 3. show login
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let loginVC: UIViewController = storyboard.instantiateViewController(withIdentifier: "loginVC") as UIViewController
                loginVC.modalPresentationStyle = .fullScreen
                self.present(loginVC, animated: true, completion: nil)
            }
        })
    }
    
    @objc func availabilities() {
        let aVC: UITableViewController = AvailabilitiesTableViewController()
        let tableview: UITableView = UITableView()
        tableview.dataSource = aVC
        tableview.delegate = aVC
        self.navigationController?.pushViewController(aVC, animated: true)
    }
    
    @objc func userInfo() {
        let userinfoVC: UIViewController = UserInfoViewController()
        self.navigationController?.pushViewController(userinfoVC, animated: true)
    }
    
    @objc func verhicleChange() {
        let vpVC: UIViewController = VerhiclePreferencesViewController()
        self.navigationController?.pushViewController(vpVC, animated: true)
    }
      
    private func getPerson() {
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        ServiceManager.getUser(token: token).responseData(completionHandler: { response in
            guard let jsonData = response.data else { return }
            guard let httpCode = response.response?.statusCode else { return }
            
            if httpCode == 200 {
                let decoder = JSONDecoder()
                do {
                    let deliverer  = try
                        decoder.decode(DelivererModel.self, from: jsonData)
                    self.deliverer = deliverer
                    // save person data to keychain.
                    KeychainWrapper.standard.set(jsonData, forKey: "person")
                    print(deliverer)
                    self.setData()
                } catch let error {
                    print(error)
                }
            } else {
                self.personName.text = "\(httpCode)"
            }
        })
    }
    
    private func setupAvailabilities() {
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        // getting data
        ServiceManager.getAvailabilities(token: token).responseJSON(completionHandler: { response in
            guard let jsonData = response.data else { return }
            
            let decoder = JSONDecoder()
            do {
                let _ = try
                    decoder.decode([AvailabilityModel].self, from: jsonData)
            } catch let error {
                print(error)
            }
        })
    }
    
    private func setData(){
        if deliverer.vehicle == 1 { personVerhicleType.text = "Fiets" }
        else if deliverer.vehicle == 2 { personVerhicleType.text = "Scooter" }
        else if deliverer.vehicle == 3 { personVerhicleType.text = "Motor" }
        else if deliverer.vehicle == 4 { personVerhicleType.text = "Auto" }
        else { personVerhicleType.text = "Unknown" }
        personFare.text = "\(String(format: "€ %.2f", deliverer.fare))"
        personName.text = deliverer.firstName
        personTotalEarnings.text = "\(String(format: "€ %.2f", deliverer.totalEarnings!))"
    }
    
    private func setUserImage() {
        if let imageData = KeychainWrapper.standard.data(forKey: "userImage") {
            let userImage = UIImage(data: imageData)
            profilePicture.image = userImage
            profilePicture.contentMode = .scaleAspectFill
        }
    }
    
    private func getAvailabilityData() {
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        ServiceManager.getAvailabilities(token: token).responseJSON(completionHandler: { response in
            guard let jsonData = response.data else {return}
            
            let decoder = JSONDecoder()
            do {
                let result = try
                    decoder.decode([AvailabilityModel].self, from: jsonData)
                self.list = result
                self.populateAvailabilityTable()
            } catch let error {
                print(error)
                self.populateAvailabilityTable()
            }
        })
    }
    
    private func populateAvailabilityTable() {
        avList = []
        for (dayIndex, _) in weekDays.enumerated(){
            let secondsInDay = ((60 * 60)*24)
            var addedAvFlag = false

            let date = Date()
            let format = DateFormatter()
            format.dateFormat = "dd-MM-yyyy"

            let calendar = Calendar.current
            let currentDate = format.string(from: date + TimeInterval(secondsInDay*dayIndex))
            do {
                try (list.sort(by: { $0.startTime < $1.startTime }))
            }
            catch let error{
                print(error)
            }
            
            for (index, _) in list.enumerated() {
                    let model = list[index]
                    let jsonFormatter = DateFormatter()
                    jsonFormatter.timeZone = .current
                    jsonFormatter.locale = Locale(identifier: "en_US_POSIX")
                    jsonFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss"
                    let jsonDate = jsonFormatter.date(from: model.date) ?? date

                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd-MM-yyyy"

                    let avdate = formatter.string(from: jsonDate)
                    if (avdate == currentDate){
                        let subStringStart = model.startTime.dropLast(3)
                        let subStringEnd = model.endTime.dropLast(3)
                        let newStartTime = String(subStringStart)
                        let newEndTime = String(subStringEnd)
                        print(subStringStart)
                        if(!addedAvFlag){
                        avList += [avDay(day: weekDays[calendar.component(.weekday, from: date + TimeInterval(secondsInDay*dayIndex))-1], startTime: newStartTime, endTime: newEndTime)]
                            addedAvFlag = true
                        }
                        else {
                            avList += [avDay(day: " ", startTime: newStartTime, endTime: newEndTime)]
                        }
                    }
            }
            if(!addedAvFlag){
                avList += [avDay(day: weekDays[calendar.component(.weekday, from: date + TimeInterval(secondsInDay*dayIndex))-1], startTime: "-", endTime: "-")]
            }
        }
        availabilityTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return avList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "avCell", for: indexPath) as! SettingsAvailabilityCell

        cell.dateLabel.text = avList[indexPath.row].day
        cell.startTimeLabel.text = avList[indexPath.row].startTime
        cell.endTimeLabel.text = avList[indexPath.row].endTime
        
        return cell
    }
    
    private func setupConstraints() {
        // Scroll view
        self.view.addSubview(scrollView)
        scrollView.snp.makeConstraints({ make in
            make.edges.equalTo(self.view)
        })
        
        scrollView.addSubview(contentView)
        contentView.snp.makeConstraints({ make in
            make.top.bottom.equalTo(scrollView)
            make.height.equalTo(750)
            make.width.equalTo(scrollView)
            make.left.right.equalTo(self.view)
            
        })
        // User view container
        contentView.addSubview(userViewContainer)
        userViewContainer.snp.makeConstraints( { make in
            make.top.equalTo(10)
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.left.equalTo(contentView.snp.left).offset(10)
           
        } )
        
        // Avatar
        userViewContainer.addSubview(profilePicture)
        profilePicture.snp.makeConstraints({ make in
            make.size.equalTo(100)
            make.left.equalTo(userViewContainer.snp.left).offset(10)
            make.top.equalTo(userViewContainer.snp.top).offset(10)
            make.bottom.equalTo(userViewContainer.snp.bottom).offset(-10)
        })
        
        // user view
        userViewContainer.addSubview(userView)
        userView.snp.makeConstraints({ make in
              make.left.equalTo(profilePicture.snp.right).offset(10)
              make.right.equalTo(userViewContainer.snp.right).offset(-10)
              make.topMargin.equalTo(userViewContainer.snp.top).offset(20)
              make.bottomMargin.equalTo(userViewContainer.snp.bottom).offset(-20)

          })
        
        // Person name
        userView.addSubview(personName)
        personName.snp.makeConstraints({ make in
            make.left.equalTo(userView.snp.left)
            make.topMargin.equalTo(userView.snp.top)
        })
        
        // Stacked view labels
        let svLabels = UIStackView(arrangedSubviews: [verhicleTypeLabel, fareLabel, totalEarningsLabel])
        svLabels.axis = .vertical
        svLabels.spacing = 5
        
        userView.addSubview(svLabels)
        svLabels.snp.makeConstraints({ make in
            make.topMargin.equalTo(personName.snp.bottomMargin).offset(10)
            make.left.equalTo(userView.snp.left)
            make.bottom.equalTo(userView.snp.bottom)
        })
        
        // Stacked view person data
        //TODO: vehicle display name
        personVerhicleType.text = "vehicle type"
        personFare.text = "€ \(deliverer.fare)"
        personTotalEarnings.text = "€ \(deliverer.totalEarnings ?? 0)"
        
        let svPerson = UIStackView(arrangedSubviews: [personVerhicleType, personFare, personTotalEarnings])
        svPerson.axis = .vertical
        svPerson.spacing = 5
        userView.addSubview(svPerson)
        svPerson.snp.makeConstraints({ make in
            make.left.equalTo(svLabels.snp.right).offset(5)
            make.bottom.equalTo(userView.snp.bottom)
        })
        
        // Buttons layout
        scrollView.addSubview(buttonsView)
        buttonsView.snp.makeConstraints({ make in
            make.top.equalTo(userViewContainer.snp.bottom).offset(10)
            make.right.equalTo(contentView).offset(-10)
            make.left.equalTo(contentView).offset(10)
        })
        
        // Info button
        buttonsView.addSubview(infoButton)
        infoButton.addTarget(self, action: #selector(userInfo), for: .touchUpInside)
        infoButton.snp.makeConstraints({ make in
            make.top.equalTo(buttonsView.snp.top)
            make.left.equalTo(buttonsView.snp.left)
            make.width.equalTo(80)
        })
        
        // Verhicle button
        buttonsView.addSubview(verhicleButton)
        verhicleButton.addTarget(self, action: #selector(verhicleChange), for: .touchUpInside)
        verhicleButton.snp.makeConstraints({ make in
            make.left.equalTo(infoButton.snp.right).offset(10)
            make.top.equalTo(buttonsView.snp.top)
            make.right.equalTo(buttonsView.snp.right)
        })
        
        // Availability button
        buttonsView.addSubview(availabilityButton)
        availabilityButton.addTarget(self, action: #selector(availabilities), for: .touchUpInside)
        availabilityButton.snp.makeConstraints({ make in
            make.left.equalTo(buttonsView.snp.left)
            make.right.equalTo(buttonsView.snp.right)
            make.top.equalTo(verhicleButton.snp.bottom).offset(10)
            make.bottom.equalTo(buttonsView.snp.bottom)
        })
        
        // Availability view
        contentView.addSubview(availabilityView)
        availabilityView.snp.makeConstraints({ make in
            make.width.equalTo(contentView)
            //make.height.equalTo(200)
            make.top.equalTo(buttonsView.snp.bottom).offset(10)
            make.bottom.equalTo(contentView)
        })
        
        availabilityView.addSubview(availabilityLabel)
        availabilityLabel.snp.makeConstraints({ make in
            make.left.equalTo(availabilityView.snp.left).offset(10)
            make.top.equalTo(availabilityView.snp.top).offset(10)
        })
        
        availabilityView.addSubview(availabilityTableView)
        availabilityTableView.snp.makeConstraints({ make in
            make.top.equalTo(availabilityLabel.snp.bottom).offset(10)
            make.right.equalTo(availabilityView.snp.right)
            make.left.equalTo(availabilityView.snp.left)
            //make.bottom.equalTo(availabilityView.snp.bottom)
        })
        availabilityView.addSubview(availabilityBottomLabel)
        availabilityBottomLabel.snp.makeConstraints({ make in
            make.top.equalTo(availabilityTableView.snp.bottom)
            make.bottom.equalTo(availabilityView.snp.bottom)
            make.right.equalTo(availabilityView.snp.right)
            make.left.equalTo(availabilityView.snp.left)
        })
    }
}
