//
//  ViewController.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 12/11/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import SnapKit

class LoginController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var usernameTextfield: UITextField!
    @IBOutlet weak var passwordLabel: UILabel!
    let spinner = ReusableComponents.spinner()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let center: NotificationCenter = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        hideKeyboard()
        initSetup()
    }
    
    func initSetup() {
        usernameLabel.text = NSLocalizedString("email", comment: "email")
        passwordLabel.text = NSLocalizedString("password", comment: "password")
        loginButton.titleLabel?.text = NSLocalizedString("login", comment: "login")
        usernameTextfield.delegate = self
        passwordTextfield.delegate = self
        passwordTextfield.isSecureTextEntry = true
        usernameTextfield.tag = 0
        passwordTextfield.tag = 1
        
        self.view.addSubview(spinner)
        spinner.snp.makeConstraints({ make in
            make.top.equalTo(loginButton.snp.bottom).offset(10)
            make.left.equalTo(self.view.snp.left).offset(20)
            make.right.equalTo(self.view.snp.right).offset(-20)
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        spinner.startAnimating()
        if let username = usernameTextfield.text {
            if let password = passwordTextfield.text {
                Validating(username: username, password: password)
            }
        }
    }
    
    func Validating(username: String, password: String) {
        if(username == "" || password == ""){
            alert(title: NSLocalizedString("field_empty_title", comment: ""), message: NSLocalizedString("field_empty_message", comment: ""))
            spinner.stopAnimating()
        } else {
            ServiceManager.login(email: username, password: password).responseString(completionHandler: { (response) in
                
                let httpCode = response.response?.statusCode
                print(httpCode!)
                if httpCode == 500 {
                    self.spinner.stopAnimating()
                    self.alert(title: NSLocalizedString("wrong_credentials_title", comment: ""), message: NSLocalizedString("wrong_credentials_message", comment: ""))
                } else if httpCode == 200 {
                    let token = response.result.value
                    self.spinner.stopAnimating()
                    // set token
                    KeychainWrapper.standard.set(token!, forKey: "token")
                    
                    //dismiss login screen
                    self.dismiss(animated: true, completion: nil)
                    
                } else if httpCode == 401 {
                    self.alert(title: NSLocalizedString("loggin_already_title", comment: ""), message: NSLocalizedString("loggin_already_message", comment: ""))
                    self.spinner.stopAnimating()
                } else {
                    self.spinner.stopAnimating()
                    self.alert(title: NSLocalizedString("connection_error_title", comment: ""), message: NSLocalizedString("connection_error_message", comment: ""))
                    self.spinner.stopAnimating()
                }
            })
        }
    }
    
    func alert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: "ok"), style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height/2
            }
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        })
    }
    
    func hideKeyboard() {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
    }
}
