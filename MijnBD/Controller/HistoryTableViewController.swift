//
//  HistoryTableViewController.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 17/11/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit
import SwiftKeychainWrapper

class HistoryTableViewController: UITableViewController {
    var list: [HistoryModel] = []
    lazy var refControl = UIRefreshControl()
    let spinner = ReusableComponents.spinner()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
        setupRefresh()
        getOrderData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getOrderData()
    }
    
    private func getOrderData() {
        spinner.startAnimating()
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        ServiceManager.getOrderHistory(token: token).responseJSON(completionHandler: { response in
            guard let jsonData = response.data else {return}
            guard let httpCode = response.response?.statusCode else { return }
            
            if httpCode == 200 {
                let decoder = JSONDecoder()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                decoder.dateDecodingStrategy = .formatted(dateFormatter)
                do {
                    let result = try
                        decoder.decode([HistoryModel].self, from: jsonData)
                    self.list = result
                    self.tableView.reloadData()
                    self.refControl.endRefreshing()
                    self.spinner.stopAnimating()
                } catch let error {
                    print(error)
                }
            } else if httpCode == 204 {
                self.list = []
                self.tableView.reloadData()
                self.refControl.endRefreshing()
                self.spinner.stopAnimating()
            } else {
                self.alert(title: NSLocalizedString("connection_error_title", comment: ""), message: NSLocalizedString("connection_error_message", comment: ""))
                self.refControl.endRefreshing()
                self.spinner.stopAnimating()
            }
            
        })
    }
    
    private func setupRefresh() {
        refControl.tintColor = .white
        refControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.refreshControl = refControl
    }
    
    func alert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: "ok"), style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func refresh() {
        getOrderData()
    }
    
    private func initSetup() {
        // Styling
        self.navigationItem.title = NSLocalizedString("history_title", comment: "")
        self.navigationController?.navigationBar.barTintColor = .systemYellow
        self.tableView.backgroundColor = .black
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        
        // Register History cell
        let cellNib = UINib(nibName: "HistoryCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: "historyCell")
        
        // setup Spinner
        spinner.style = .gray
        let barButton = UIBarButtonItem(customView: spinner)
        self.navigationItem.setLeftBarButton(barButton, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if list.count == 0 {
            tableView.setEmptyView(title: NSLocalizedString("history_empty_title", comment: ""), message: NSLocalizedString("history_empty_message", comment: ""))
        } else {
            tableView.restore()
        }
        return list.count;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath) as! HistoryCell
        let model = list[indexPath.row]
        cell.priceLabel.text = String(format: "€ %.2f", model.price)
        cell.iconVehicle.image = getVehicleImage(vehicle: model.vehicle)
        cell.iconCheck.image = getStatusIcon(status: model.status)
        cell.addressLabel.text = model.customer.address
        
        let startTime = getTime(date: model.warehousePickUpAt)
        let deliveredTime = getTime(date: model.deliveredAt)
        cell.timeLabel.text = "\(startTime) - \(deliveredTime)"
        cell.distanceLabel.text = getRange(a: model.customerDistanceInKilometers, b: model.warehouseDistanceInKilometers)
        cell.durationLabel.text = timeDiff(pickedUp: model.warehousePickUpAt, deliverdAt: model.deliveredAt)
        
        return cell
    }
    
    private func getTime(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSSSSS"
        let date_d = dateFormatter.date(from: date)!
        
        let stringFormatter = DateFormatter()
        stringFormatter.dateFormat = "HH:mm"
        let time = stringFormatter.string(from: date_d)
        return time
    }
    
    private func timeDiff(pickedUp: String, deliverdAt: String) -> String {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSSSSS"
        let date_pickedUp = dateFormatter1.date(from: pickedUp)!
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSSSSS"
        let date_deliverdAt = dateFormatter2.date(from: deliverdAt)!
        
        let calendar = Calendar.current
        let d1 = calendar.startOfDay(for: date_pickedUp)
        let d2 = calendar.startOfDay(for: date_deliverdAt)
        let components = calendar.dateComponents([.minute, .hour], from: d1, to: d2)
        
        return "\(components.minute!) min."
    }
    
    private func getRange(a: Double, b: Double) -> String {
        let range = a + b
        return String(format: "%.1f km.", range)
    }
    
    private func getVehicleImage(vehicle: Int) -> UIImage {
        var image = "ic_car_white"
        if vehicle == 1 {
            image = "ic_bike_white"
        } else if vehicle == 2 || vehicle == 3 {
            image = "ic_motor_white"
        }
        let img = UIImage(named: image)
        return img!
    }
    
    private func getStatusIcon(status: Int) -> UIImage {
        var image = "ic_done_24px"
        if status != 4 {
            image = "ic_close_24px"
        }
        let img = UIImage(named: image)
        return img!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = list[indexPath.row]
        let vc = HistoryDetailsViewController(nibName: "HistoryDetails", bundle: nil)
        vc.model = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
}
extension UITableView {
    func setEmptyView(title: String, message: String) {
        let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
        let titleLabel = UILabel()
        let messageLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 17)
        emptyView.addSubview(titleLabel)
        emptyView.addSubview(messageLabel)
        titleLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
        messageLabel.leftAnchor.constraint(equalTo: emptyView.leftAnchor, constant: 20).isActive = true
        messageLabel.rightAnchor.constraint(equalTo: emptyView.rightAnchor, constant: -20).isActive = true
        titleLabel.text = title
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        // The only tricky part is here:
        self.backgroundView = emptyView
        self.separatorStyle = .none
    }
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}
