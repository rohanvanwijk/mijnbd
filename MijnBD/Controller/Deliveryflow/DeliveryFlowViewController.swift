//
//  DeliveryFlowViewController.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 10/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import SwiftKeychainWrapper
import CoreLocation

class DeliveryFlowViewController: UIViewController {
    let callButton: UIButton = {
       let button = UIButton()
        button.layer.cornerRadius = 10
        button.tintColor = .black
        button.backgroundColor = .white
        button.setTitleColor(.black, for: .normal)
        button.setTitle("Bel magazijn", for: .normal)
        return button
    }()
    let cancelButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 10
        button.backgroundColor = .white
        button.setTitleColor(.black, for: .normal)
        button.setTitle("Annuleer", for: .normal)
        return button
    }()
    let deliveryView: UIView = {
        let view = UIView()
        view.backgroundColor = .darkGray
        view.layer.cornerRadius = 20
        return view
    }()
    let deliveryLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Haal de bezorging op bij het maggazijn"
        label.textColor = .white
        return label
    }()
    let warehouseAdress: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 27)
        label.numberOfLines = 0
        return label
    }()
    let navigateButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .systemYellow
        button.layer.cornerRadius = 10
        button.setTitleColor(.black, for: .normal)
        button.setTitle("Open route in maps", for: .normal)
        return button
    }()
    let gotDeliveryButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.layer.cornerRadius = 10
        button.setTitleColor(.black, for: .normal)
        button.setTitle("Bestelling opgehaald", for: .normal)
        return button
    }()
    let deliveryTimeView: UIView = {
        let view = UIView()
        view.backgroundColor = .darkGray
        view.layer.cornerRadius = 20
        return view
    }()
    let deliveryTime: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .white
        return label
    }()
    let customerView: UIView = {
        let view = UIView()
        view.backgroundColor = .darkGray
        view.layer.cornerRadius = 20
        return view
    }()
    
    var atWarehouse = false
    
    public var notification: NotificationModel?
    public var delivery: DeliveryModel?
    let locMan = CLLocationManager()
    var currentLocation: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
        getCurrentLocation()
    }
    
    private func initSetup() {
        // Styling
        self.navigationItem.title = "Bezorgen"
        self.navigationController?.navigationBar.barTintColor = .systemYellow
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.view.backgroundColor = .black
        
        setupStyling()
    }
    
    private func setupStyling() {
        let callButton1 = ReusableComponents.Button(text: "Bel magazijn", icon: "", color: .white)
        let cancelButton1 = ReusableComponents.Button(text: "Annuleer", icon: "", color: .white)
        
        //setup buttons
        let svButtons = UIStackView(arrangedSubviews: [callButton1, cancelButton1])
        svButtons.axis = .horizontal
        svButtons.spacing = 10
        svButtons.distribution = .fillEqually
        self.view.addSubview(svButtons)
        svButtons.snp.makeConstraints({ make in
            make.top.equalTo(self.view.snp.top).offset(100)
            make.left.equalTo(self.view.snp.left).offset(10)
            make.right.equalTo(self.view.snp.right).offset(-10)
            make.height.equalTo(60)
        })
        
        cancelButton.addTarget(self, action: #selector(cancelDelivery), for: .touchUpInside)
        callButton.addTarget(self, action: #selector(callWarehouse), for: .touchUpInside)
        
        self.view.addSubview(deliveryView)
        deliveryView.snp.makeConstraints({ make in
            make.top.equalTo(svButtons.snp.bottom).offset(20)
            make.left.equalTo(self.view.snp.left).offset(10)
            make.right.equalTo(self.view.snp.right).offset(-10)
            make.height.equalTo(400)
        })
        
        deliveryView.addSubview(deliveryLabel)
        deliveryLabel.snp.makeConstraints({ make in
            make.top.equalTo(deliveryView.snp.top).offset(30)
            make.left.equalTo(deliveryView.snp.left).offset(30)
            make.right.equalTo(deliveryView.snp.right).offset(-30)
        })
        
        // warehouse addres
        if let warehouseAdd = delivery?.warehouse.address {
            warehouseAdress.text = warehouseAdd
            
            deliveryView.addSubview(warehouseAdress)
            warehouseAdress.snp.makeConstraints({ make in
                make.top.equalTo(deliveryLabel.snp.bottom).offset(30)
                make.left.equalTo(deliveryView.snp.left).offset(30)
                make.right.equalTo(deliveryView.snp.right).offset(-30)
            })
            
            deliveryView.addSubview(navigateButton)
            navigateButton.addTarget(self, action: #selector(openRoute), for: .touchUpInside)
            navigateButton.snp.makeConstraints({ make in
                make.top.equalTo(warehouseAdress.snp.bottom).offset(30)
                make.left.equalTo(deliveryView.snp.left).offset(30)
                make.right.equalTo(deliveryView.snp.right).offset(-30)
            })
        }
        
        deliveryView.addSubview(gotDeliveryButton)
        gotDeliveryButton.addTarget(self, action: #selector(gotDelivery), for: .touchUpInside)
        gotDeliveryButton.snp.makeConstraints({ make in
            make.top.equalTo(navigateButton.snp.bottom).offset(30)
            make.left.equalTo(deliveryView.snp.left).offset(30)
            make.right.equalTo(deliveryView.snp.right).offset(-30)
        })
        
        self.view.addSubview(deliveryTimeView)
        deliveryTimeView.snp.makeConstraints({ make in
            make.bottom.equalTo(self.view.snp.bottom).offset(-60)
            make.left.equalTo(self.view.snp.left).offset(10)
            make.right.equalTo(self.view.snp.right).offset(-10)
            make.height.equalTo(100)
        })
        
        deliveryTimeView.addSubview(deliveryTime)
        if let dueDate = delivery?.dueDate {
            let jsonFormatter = DateFormatter()
            jsonFormatter.timeZone = .current
            jsonFormatter.locale = Locale(identifier: "en_US_POSIX")
            jsonFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss"
            let jsonDate = jsonFormatter.date(from: dueDate)!
            
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            let date = formatter.string(from: jsonDate)
            
            deliveryTime.text = "Uiterlijke levertijd: \(date)"
        }
        deliveryTime.snp.makeConstraints({ make in
            make.center.equalTo(deliveryTimeView)
        })
    }
    
    @objc func cancelDelivery() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func callWarehouse() {
        var phone = "03283"
        if atWarehouse {
            phone = delivery!.customerPhoneNumber
        }
        let number = URL(string: "tel://\(phone)")
        UIApplication.shared.open(number!, options: [:], completionHandler: nil)
    }
    
    @objc func openRoute() {
        var long = delivery!.warehouse.longitude
        var lat = delivery!.warehouse.latitude
        var vehicle = "bicycle"
        if atWarehouse {
            long = delivery!.customer.longitude
            lat = delivery!.customer.latitude
        }
        let url = URL(string: "comgooglemaps://?saddr=&daddr=\(lat),\(long)&directionsmode=\(vehicle)")!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @objc func gotDelivery() {
        if atWarehouse {
            // api call set status to 4
            delivered()
            let vc = DeliverySuccessViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        atWarehouse = true
        createCustomerView()
    }
    
    private func createCustomerView() {
        deliveryLabel.text = "Bezorg de bestelling bij de klant"
        if let adress = delivery?.customer.address {
            warehouseAdress.text = adress
        }
        gotDeliveryButton.setTitle("Bestelling afgeleverd", for: .normal)
        callButton.setTitle("Bel klant", for: .normal)
    }
    
    private func delivered() {
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        guard let dId = notification?.id else { return }
        
        ServiceManager.updateDelivery(deliveryId: dId, status: 4, token: token, currentLocation: currentLocation).responseData(completionHandler: { response in
            guard let jsonData = response.data else {return}
            let httpCode = response.response?.statusCode
              
            if httpCode == 200 {
              do {
                  let decoder = JSONDecoder()
                  let result = try
                      decoder.decode(DeliveryModel.self, from: jsonData)
              } catch {
                  print(error)
              }
            }
        })
    }
    
    private func getCurrentLocation() {
           if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways {
               currentLocation = locMan.location
               print("long: \(currentLocation.coordinate.longitude), lat: \(currentLocation.coordinate.latitude)")
           }
       }
    
    private func updateDeliveryStatus(notification: NotificationModel, token: String) {
          // update delivery status
         ServiceManager.updateDelivery(deliveryId: notification.id, status: 3, token: token, currentLocation: currentLocation).responseData(completionHandler: { response in
          guard let jsonData = response.data else {return}
             let httpCode = response.response?.statusCode
             
             if httpCode == 200 {
                 do {
                     let decoder = JSONDecoder()
                     let result = try
                         decoder.decode(DeliveryModel.self, from: jsonData)
                     print(result)
                 } catch {
                     print(error)
                 }
             }
         })
      }
}
