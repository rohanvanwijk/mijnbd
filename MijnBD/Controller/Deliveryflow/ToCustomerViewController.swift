//
//  ToCustomer.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 19/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit
import MTSlideToOpen
import SwiftKeychainWrapper
import CoreLocation

class ToCustomerViewController: UIViewController, MTSlideToOpenDelegate {
    public var NOTIFICATION: NotificationModel?
    public var DELIVERY: DeliveryModel?
    let locMan = CLLocationManager()
    let spinner = ReusableComponents.spinner()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
        setupStyling()
    }
    
    private func initSetup() {
        // Styling
        self.navigationItem.title = "Bezorgen"
        self.navigationController?.navigationBar.barTintColor = .systemYellow
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.view.backgroundColor = .black
        self.navigationItem.hidesBackButton = true
    }
    
    @objc func openRoute() {
        let long = DELIVERY!.customer.longitude
        let lat = DELIVERY!.customer.latitude
        var vehicle = "bicycle"
        if DELIVERY?.vehicle != 1 {
            vehicle = "driving"
        }
        let url = URL(string: "comgooglemaps://?saddr=&daddr=\(lat),\(long)&directionsmode=\(vehicle)")!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @objc func callTo() {
        let phone = DELIVERY!.customerPhoneNumber
        let number = URL(string: "tel://\(phone)")
        UIApplication.shared.open(number!, options: [:], completionHandler: nil)
    }
    
    func mtSlideToOpenDelegateDidFinish(_ sender: MTSlideToOpenView) {
        spinner.startAnimating()
        guard let del = DELIVERY else { return }
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        guard let cl = getCurrentLocation() else { return }
        
        ServiceManager.updateDelivery(deliveryId: del.id, status: 4, token: token, currentLocation: cl).responseData(completionHandler: { response in
            let httpcode = response.response?.statusCode
            if httpcode == 200 {
                let vc = DeliverySuccessViewController()
                vc.DELIVERY = del
                vc.NOTIFICATION = self.NOTIFICATION
                self.navigationController?.pushViewController(vc, animated: true)
                self.spinner.stopAnimating()
            } else {
                self.alert(title: NSLocalizedString("connection_error_title", comment: ""), message: NSLocalizedString("connection_error_message", comment: ""))
                self.spinner.stopAnimating()
                sender.resetStateWithAnimation(true)
            }
        })
    }
    
    private func setupStyling() {
        guard let model = DELIVERY else { return }
        
        let callButton = ReusableComponents.Button(text: "Bel klant", icon: "ic_call_24px", color: .white)
        callButton.addTarget(self, action: #selector(callTo), for: .touchUpInside)
        let cancelButton = ReusableComponents.Button(text: "Annuleer", icon: "iconClose", color: .white)
        cancelButton.addTarget(self, action: #selector(cancelDelivery), for: .touchUpInside)
        
        //setup buttons
        let svButtons = UIStackView(arrangedSubviews: [callButton, cancelButton])
        svButtons.axis = .horizontal
        svButtons.spacing = 10
        svButtons.distribution = .fillEqually
        self.view.addSubview(svButtons)
        svButtons.snp.makeConstraints({ make in
            make.top.equalTo(self.view.snp.top).offset(100)
            make.left.equalTo(self.view.snp.left).offset(10)
            make.right.equalTo(self.view.snp.right).offset(-10)
            make.height.equalTo(60)
        })
        
        let labelStatus = ReusableComponents.alertMessage(text: "Bestelling opgehaald", icon: "ic_warehouse")
        labelStatus.contentEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        self.view.addSubview(labelStatus)
        labelStatus.snp.makeConstraints({ make in
            make.top.equalTo(svButtons.snp.bottom).offset(20)
            make.left.equalTo(self.view.snp.left).offset(10)
            make.right.equalTo(self.view.snp.right).offset(-10)
        })
        
        let foccusedView = ReusableComponents.focussedView()
        self.view.addSubview(foccusedView)
        foccusedView.snp.makeConstraints({ make in
            make.top.equalTo(labelStatus.snp.bottom).offset(20)
            make.left.equalTo(self.view.snp.left).offset(10)
            make.right.equalTo(self.view.snp.right).offset(-10)
        })
        
        let image = ReusableComponents.image(icon: "ic_home_white")
        let labelPickup = ReusableComponents.label(text: "Bezorg de bestelling", color: 0)
        foccusedView.addSubview(image)
        foccusedView.addSubview(labelPickup)
        image.snp.makeConstraints({ make in
            make.top.equalTo(foccusedView.snp.top).offset(20)
            make.left.equalTo(foccusedView.snp.left).offset(20)
        })
        labelPickup.snp.makeConstraints({ make in
            make.top.equalTo(foccusedView.snp.top).offset(20)
            make.left.equalTo(image.snp.right).offset(20)
            make.right.equalTo(foccusedView.snp.right).offset(-20)
        })
        
        let svAdres = ReusableComponents.adressSV(adres: model.customer.address)
        foccusedView.addSubview(svAdres)
        svAdres.snp.makeConstraints({ make in
            make.top.equalTo(labelPickup.snp.bottom).offset(20)
            make.left.equalTo(foccusedView.snp.left).offset(20)
            make.right.equalTo(foccusedView.snp.right).offset(-20)
            
        })
        
        let buttonMaps = ReusableComponents.Button(text: "Open route in maps", icon: "iconMaps", color: .systemYellow)
        buttonMaps.contentEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        buttonMaps.addTarget(self, action: #selector(openRoute), for: .touchUpInside)
        foccusedView.addSubview(buttonMaps)
        buttonMaps.snp.makeConstraints({ make in
            make.top.equalTo(svAdres.snp.bottom).offset(20)
            make.left.equalTo(foccusedView.snp.left).offset(20)
            make.right.equalTo(foccusedView.snp.right).offset(-20)
        })
        
        let slide = ReusableComponents.slider(text: "Bestelling afgeleverd")
        slide.delegate = self
        foccusedView.addSubview(slide)
        slide.snp.makeConstraints({ make in
            make.top.equalTo(buttonMaps.snp.bottom).offset(20)
            make.left.equalTo(foccusedView.snp.left).offset(20)
            make.right.equalTo(foccusedView.snp.right).offset(-20)
        })
        
        foccusedView.addSubview(spinner)
        spinner.snp.makeConstraints({ make in
            make.top.equalTo(slide.snp.bottom).offset(10)
            make.left.equalTo(foccusedView.snp.left).offset(20)
            make.right.equalTo(foccusedView.snp.right).offset(-20)
        })
        
        let dueTime = getTimeString(dateString: model.dueDate)
        let dueDateView = ReusableComponents.label(text: "\(dueTime) uiterlijke levertijd", color: 0)
        self.view.addSubview(dueDateView)
        dueDateView.snp.makeConstraints({ make in
            make.top.equalTo(spinner.snp.bottom).offset(20)
            make.left.equalTo(foccusedView.snp.left).offset(20)
            make.right.equalTo(foccusedView.snp.right).offset(-20)
            make.bottom.equalTo(foccusedView.snp.bottom).offset(-20)
        })
    }
    
    private func getTimeString(dateString: String) -> String {
        let jsonFormatter = DateFormatter()
        jsonFormatter.timeZone = .current
        jsonFormatter.locale = Locale(identifier: "en_US_POSIX")
        jsonFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss"
        if let jsonDate = jsonFormatter.date(from: dateString) {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            let time = formatter.string(from: jsonDate)
            return time
        }
        return ""
    }
    
    @objc func cancelDelivery() {
        guard let del = DELIVERY else { return }
        let vc = CancelDeliveryViewController(nibName: "CancelDeliveryView", bundle: nil)
        vc.DELIVERY = del
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func getCurrentLocation() -> CLLocation? {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways {
            let currentLocation = locMan.location
            return currentLocation
        }
        return nil
    }
    
    func alert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: "ok"), style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
