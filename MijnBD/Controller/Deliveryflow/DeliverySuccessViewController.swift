//
//  DeliverySuccessViewController.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 11/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import SwiftKeychainWrapper

class DeliverySuccessViewController: UIViewController {
    
    public var DELIVERY: DeliveryModel?
    public var NOTIFICATION: NotificationModel?
    private var HISTORYMODEL: DeliveryModel?
    private var acceptedAt: String?
    private var createdAt: String?
    
    // Outlets
    @IBOutlet weak var labelDeliveryDate: UILabel!
    @IBOutlet weak var viewEarnings: UIView!
    @IBOutlet weak var labelVehicleEarning: UILabel!
    @IBOutlet weak var viewTotalEarnings: UIView!
    @IBOutlet weak var labelEarningToday: UILabel!
    @IBOutlet weak var labelEarningWeek: UILabel!
    @IBOutlet weak var labelEarningMonth: UILabel!
    @IBOutlet weak var buttonDetails: UIButton!
    @IBOutlet weak var buttonNewDelivery: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
        setHistoryModel()
    }
    
    private func initSetup() {
        guard let MODEL = DELIVERY else { return }
        // Styling
        self.navigationItem.title = "Bezorging succesvol"
        self.navigationController?.navigationBar.barTintColor = .systemYellow
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.view.backgroundColor = .black
        self.navigationItem.hidesBackButton = true
        
        let succesView = UINib(nibName: "DeliverySuccesView", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
        self.view.addSubview(succesView)
        succesView.snp.makeConstraints({ make in
            make.top.equalTo(self.view.snp.top)
            make.left.equalTo(self.view.snp.left)
            make.right.equalTo(self.view.snp.right)
            make.bottom.equalTo(self.view.snp.bottom)
        })
        
        // fill data from Model
        let price = String(format: "%.2f", MODEL.price)
        viewEarnings.layer.cornerRadius = 20
        viewTotalEarnings.layer.cornerRadius = 20
        labelVehicleEarning.text = "€ \(price) levering \(mapVehicle(_vehicle: MODEL.vehicle))"
        labelDeliveryDate.text = ""
        
        buttonDetails.alignTextUnderImage(spacing: 5)
        buttonNewDelivery.alignTextUnderImage(spacing: 5)
        buttonDetails.addTarget(self, action: #selector(details), for: .touchUpInside)
        buttonNewDelivery.addTarget(self, action: #selector(dismissDeliveryFlow), for: .touchUpInside)
        buttonDetails.layer.borderColor = UIColor.systemYellow.cgColor
        buttonDetails.layer.borderWidth = 1
        buttonNewDelivery.layer.borderColor = UIColor.systemYellow.cgColor
        buttonNewDelivery.layer.borderWidth = 1
        
        setEarningsToday()
        setEarningsWeek()
        setEarningsMonth()
    }
    
    private func getTimeString(dateString: String, format: String) -> String {
        let jsonFormatter = DateFormatter()
        jsonFormatter.timeZone = .current
        jsonFormatter.locale = Locale(identifier: "en_US_POSIX")
        jsonFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSSSSS'"
        let jsonDate = jsonFormatter.date(from: dateString)!
        
        let formatter = DateFormatter()
        formatter.dateFormat = format
        let time = formatter.string(from: jsonDate)
        
        return time
    }
    
    private func setEarningsToday() {
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        ServiceManager.getEarnings(token: token, timeframe: "day").responseString(completionHandler: { response in
            guard let httpcode = response.response?.statusCode else { return }
            
            if httpcode == 200 {
                let price = response.result.value
                self.labelEarningToday.text = "€ \(price!) vandaag"
            } else {
                let price = self.DELIVERY!.price
                self.labelEarningToday.text = "€ \(String(format: "%.2f", price)) vandaag"
            }
        })
    }
    
    private func setEarningsWeek() {
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        ServiceManager.getEarnings(token: token, timeframe: "week").responseString(completionHandler: { response in
            guard let httpcode = response.response?.statusCode else { return }
            
            if httpcode == 200 {
                let price = response.result.value
                self.labelEarningWeek.text = "€ \(price!) week"
            } else {
                let price = self.DELIVERY!.price
                self.labelEarningWeek.text = "€ \(String(format: "%.2f", price)) week"
            }
        })
    }
    
    private func setEarningsMonth() {
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        ServiceManager.getEarnings(token: token, timeframe: "month").responseString(completionHandler: { response in
            guard let httpcode = response.response?.statusCode else { return }
            
            if httpcode == 200 {
                let price = response.result.value
                self.labelEarningMonth.text = "€ \(price!) maand"
            } else {
                let price = self.DELIVERY!.price
                self.labelEarningMonth.text = "€ \(String(format: "%.2f", price)) maand"
            }
        })
    }
    
    @objc func details() {
        guard let hModel = HISTORYMODEL else { return }
        
        let model = HistoryModel(id: hModel.id, delivererID: hModel.delivererID!, customerPhoneNumber: hModel.customerPhoneNumber, dueDate: hModel.dueDate, vehicle: hModel.vehicle, startedAtID: hModel.startedAtID!, warehouseDistanceInKilometers: hModel.warehouseDistanceInKilometers!, warehouseETA: hModel.warehouseETA!, warehouseID: hModel.warehouseID, warehousePickUpAt: hModel.warehousePickUpAt!, customerDistanceInKilometers: hModel.customerDistanceInKilometers!, customerETA: hModel.customerETA!, customerID: hModel.customerID, currentID: hModel.currentID!, deliveredAt: hModel.deliveredAt!, price: hModel.price, tip: hModel.tip ?? 0, paymentMethod: hModel.paymentMethod, status: hModel.status, warehouse: hModel.warehouse, customer: hModel.customer, current: hModel.current, createdAt: self.createdAt, acceptedAt: self.acceptedAt)
        
        let vc = HistoryDetailsViewController(nibName: "HistoryDetails", bundle: nil)
        vc.model = model
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    private func getAcceptedTime(deliveryId: String) {
        guard let hModel = HISTORYMODEL else { return }
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        ServiceManager.getOrderHistory(token: token).responseData(completionHandler: { response in
            guard let jsonData = response.data else { return }
            guard let httpcode = response.response?.statusCode else { return }
            
            if httpcode == 200 {
                let decoder = JSONDecoder()
                do {
                    let result = try
                        decoder.decode([HistoryModel].self, from: jsonData)
                    if let hm = result.first(where: { $0.id == hModel.id }) {
                        self.acceptedAt = hm.acceptedAt
                        self.createdAt = hm.createdAt
                        self.labelDeliveryDate.text = self.getTimeString(dateString: hm.deliveredAt, format: "HH:mm dd-MM-yyy")
                    }
                } catch {
                    print(error)
                }
            }
        })
    }
    
    private func setHistoryModel() {
        ServiceManager.getDelivery(deliveryId: DELIVERY!.id).responseData(completionHandler: { response in
            guard let jsonData = response.data else { return }
            let httpCode = response.response?.statusCode
            
            if httpCode == 200 {
                do {
                    let decoder = JSONDecoder()
                    let result = try
                        decoder.decode(DeliveryModel.self, from: jsonData)
                    self.HISTORYMODEL = result
                    self.getAcceptedTime(deliveryId: result.id)
                } catch {
                    print(error)
                }
            }
        })
    }
    
    @objc func dismissDeliveryFlow() {
        self.dismiss(animated: true, completion: {
            let vc = DeliveryViewController()
            vc.StartTimer()
        })
    }
    
    private func mapVehicle(_vehicle: Int) -> String {
        switch _vehicle {
        case 1:
            return "Fiets"
        case 2:
            return "Scooter"
        case 3:
            return "Motor"
        default:
            return "Auto"
        }
    }
}
public extension UIButton
  {
    func alignTextUnderImage(spacing: CGFloat = 6.0)
    {
        if let image = self.imageView?.image
        {
            let imageSize: CGSize = image.size
            self.titleEdgeInsets = UIEdgeInsets(top: spacing, left: -imageSize.width, bottom: -(imageSize.height), right: 0.0)
            let labelString = NSString(string: self.titleLabel!.text!)
            let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: self.titleLabel!.font])
            self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0.0, bottom: 0.0, right: -titleSize.width)
        }
    }
}
