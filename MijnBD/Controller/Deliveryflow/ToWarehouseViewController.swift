//
//  ToWarehouseViewController.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 19/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import SwiftKeychainWrapper
import CoreLocation
import MTSlideToOpen

class ToWarehouseViewController: UIViewController, MTSlideToOpenDelegate {
    
    public var NOTIFICATION: NotificationModel?
    public var DELIVERY: DeliveryModel?
    let locMan = CLLocationManager()
    let spinner = ReusableComponents.spinner()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
        setupStyling()
        // Update delivery status to 2
        updateDelivery(status: 2)
    }
    
    private func initSetup() {
        // Styling navigation
        self.navigationItem.title = "Bezorgen"
        self.navigationController?.navigationBar.barTintColor = .systemYellow
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.view.backgroundColor = .black
    }
    
    private func setupStyling() {
        guard let model = DELIVERY else { return }
        
        let callButton = ReusableComponents.Button(text: "Bel magazijn", icon: "ic_call_24px", color: .white)
        callButton.addTarget(self, action: #selector(callTo), for: .touchUpInside)
        let cancelButton = ReusableComponents.Button(text: "Annuleer", icon: "iconClose", color: .white)
        cancelButton.addTarget(self, action: #selector(cancelDelivery), for: .touchUpInside)
        
        //setup buttons
        let svButtons = UIStackView(arrangedSubviews: [callButton, cancelButton])
        svButtons.axis = .horizontal
        svButtons.spacing = 10
        svButtons.distribution = .fillEqually
        self.view.addSubview(svButtons)
        svButtons.snp.makeConstraints({ make in
            make.top.equalTo(self.view.snp.top).offset(100)
            make.left.equalTo(self.view.snp.left).offset(10)
            make.right.equalTo(self.view.snp.right).offset(-10)
            make.height.equalTo(60)
        })
        
        let foccusedView = ReusableComponents.focussedView()
        self.view.addSubview(foccusedView)
        foccusedView.snp.makeConstraints({ make in
            make.top.equalTo(svButtons.snp.bottom).offset(20)
            make.left.equalTo(self.view.snp.left).offset(10)
            make.right.equalTo(self.view.snp.right).offset(-10)
        })
        
        let image = ReusableComponents.image(icon: "ic_warehouse")
        let labelPickup = ReusableComponents.label(text: "Haal de bezorging op bij het magazijn", color: 0)
        foccusedView.addSubview(image)
        foccusedView.addSubview(labelPickup)
        image.snp.makeConstraints({ make in
            make.top.equalTo(foccusedView.snp.top).offset(30)
            make.left.equalTo(foccusedView.snp.left).offset(20)
        })
        labelPickup.snp.makeConstraints({ make in
            make.top.equalTo(foccusedView.snp.top).offset(20)
            make.left.equalTo(image.snp.right).offset(20)
            make.right.equalTo(foccusedView.snp.right).offset(-20)
        })
        
        let svAdres = ReusableComponents.adressSV(adres: model.warehouse.address)
        foccusedView.addSubview(svAdres)
        svAdres.snp.makeConstraints({ make in
            make.top.equalTo(labelPickup.snp.bottom).offset(20)
            make.left.equalTo(foccusedView.snp.left).offset(20)
            make.right.equalTo(foccusedView.snp.right).offset(-20)
            
        })
        
        let buttonMaps = ReusableComponents.Button(text: "Open route in maps", icon: "iconMaps", color: .systemYellow)
        buttonMaps.contentEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        buttonMaps.addTarget(self, action: #selector(openRoute), for: .touchUpInside)
        foccusedView.addSubview(buttonMaps)
        buttonMaps.snp.makeConstraints({ make in
            make.top.equalTo(svAdres.snp.bottom).offset(20)
            make.left.equalTo(foccusedView.snp.left).offset(20)
            make.right.equalTo(foccusedView.snp.right).offset(-20)
        })
        
        let slide = ReusableComponents.slider(text: "Bestelling opgehaald")
        slide.delegate = self
        foccusedView.addSubview(slide)
        slide.snp.makeConstraints({ make in
            make.top.equalTo(buttonMaps.snp.bottom).offset(20)
            make.left.equalTo(foccusedView.snp.left).offset(20)
            make.right.equalTo(foccusedView.snp.right).offset(-20)
        })
        
        foccusedView.addSubview(spinner)
        spinner.snp.makeConstraints({ make in
            make.top.equalTo(slide.snp.bottom).offset(10)
            make.left.equalTo(foccusedView.snp.left).offset(20)
            make.right.equalTo(foccusedView.snp.right).offset(-20)
            make.bottom.equalTo(foccusedView.snp.bottom).offset(-10)
        })
        
        let dueTime = getTimeString(dateString: model.dueDate)
        let alertView = ReusableComponents.alertMessage(text: "\(dueTime) uiterlijke levertijd", icon: "ic_home_white")
        alertView.contentEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        self.view.addSubview(alertView)
        alertView.snp.makeConstraints({ make in
            make.top.equalTo(foccusedView.snp.bottom).offset(20)
            make.bottom.equalTo(self.view.snp.bottom).offset(-90)
            make.left.equalTo(self.view.snp.left).offset(20)
            make.right.equalTo(self.view.snp.right).offset(-20)
        })
    }
    
    @objc func openRoute() {
        let long = DELIVERY!.warehouse.longitude
        let lat = DELIVERY!.warehouse.latitude
        var vehicle = "bicycle"
        if DELIVERY?.vehicle != 1 {
            vehicle = "driving"
        }
        let url = URL(string: "comgooglemaps://?saddr=&daddr=\(lat),\(long)&directionsmode=\(vehicle)")!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @objc func callTo() {
        let phone = "085-2250337"
        let number = URL(string: "tel://\(phone)")
        UIApplication.shared.open(number!, options: [:], completionHandler: nil)
    }
    
    @objc func cancelDelivery() {
        alert(title: "Wil je deze bezorging annuleren?", message: "")
    }
    
    func alert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("no", comment: ""), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment: ""), style: .default, handler: { make in
            self.cancelDeliveryToAPI()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func cancelDeliveryToAPI() {
           guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
           guard let cl = getCurrentLocation() else { return }
           guard let del = DELIVERY else { return }
           
           ServiceManager.updateDelivery(deliveryId: del.id, status: 0, token: token, currentLocation: cl).responseData(completionHandler: { response in
               guard let httpcode = response.response?.statusCode else { return }
               if httpcode == 200 {
                self.navigationController?.dismiss(animated: true, completion: nil)
               }
           })
       }
    
    private func getTimeString(dateString: String) -> String {
        let jsonFormatter = DateFormatter()
        jsonFormatter.timeZone = .current
        jsonFormatter.locale = Locale(identifier: "en_US_POSIX")
        jsonFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss"
        if let jsonDate = jsonFormatter.date(from: dateString) {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            let time = formatter.string(from: jsonDate)
            return time
        }
        return ""
    }
    
    func mtSlideToOpenDelegateDidFinish(_ sender: MTSlideToOpenView) {
        spinner.startAnimating()
        guard let del = DELIVERY else { return }
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        guard let cl = getCurrentLocation() else { return }
        
        print("delivery id: \(del.id)")
        ServiceManager.updateDelivery(deliveryId: del.id, status: 3, token: token, currentLocation: cl).responseData(completionHandler: { response in
            let httpcode = response.response?.statusCode
            
            if httpcode == 200 {
                let vc = ToCustomerViewController()
                vc.DELIVERY = del
                vc.NOTIFICATION = self.NOTIFICATION
                self.navigationController?.pushViewController(vc, animated: true)
                self.spinner.stopAnimating()
            } else {
                self.alert(title: NSLocalizedString("connection_error_title", comment: ""), message: NSLocalizedString("connection_error_message", comment: ""))
                self.spinner.stopAnimating()
                sender.resetStateWithAnimation(true)
            }
        })
    }
    
    private func updateDelivery(status: Int) {
        guard let del = DELIVERY else { return }
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        guard let cl = getCurrentLocation() else { return }
        
        ServiceManager.updateDelivery(deliveryId: del.id, status: status, token: token, currentLocation: cl).responseData(completionHandler: { response in
            guard let code = response.response?.statusCode else { return }
            print("patch \(code)")
            print("update delivery status...")
        })
    }
    
    private func getCurrentLocation() -> CLLocation? {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways {
            let currentLocation = locMan.location
            return currentLocation
        }
        return nil
    }
}
