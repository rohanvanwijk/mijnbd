//
//  CancelDeliveryViewController.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 24/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import SwiftKeychainWrapper
import CoreLocation

class CancelDeliveryViewController: UIViewController {
    // Outlets
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textViewReason: UITextView!
    @IBOutlet weak var buttonSubmit: UIButton!
    
    let locMan = CLLocationManager()
    public var DELIVERY: DeliveryModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
    }
    
    private func initSetup() {
        // Styling navigation
        self.navigationItem.title = NSLocalizedString("cancel_delivery_title", comment: "")
        self.navigationController?.navigationBar.barTintColor = .systemYellow
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.view.backgroundColor = .black
        self.navigationController?.navigationBar.tintColor = .black
        
        labelTitle.text = NSLocalizedString("cancel_delivery_subtitle", comment: "")
        textViewReason.textColor = .darkGray
        buttonSubmit.setTitle(NSLocalizedString("cancel_delivery_submit", comment: ""), for: .normal)
        buttonSubmit.addTarget(self, action: #selector(submit), for: .touchUpInside)
    }
    
    @objc func submit() {
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        guard let cl = getCurrentLocation() else { return }
        guard let del = DELIVERY else { return }
        
        ServiceManager.updateDelivery(deliveryId: del.id, status: 0, token: token, currentLocation: cl).responseData(completionHandler: { response in
            guard let httpcode = response.response?.statusCode else { return }
            if httpcode == 200 {
                let vc = ReturnToWarehouse()
                vc.DELIVERY = del
                self.navigationController?.pushViewController(vc, animated: true)
            }
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    private func getCurrentLocation() -> CLLocation? {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways {
            let currentLocation = locMan.location
            return currentLocation
        }
        return nil
    }
}
