//
//  ReturnToWarehouse.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 17/01/2020.
//  Copyright © 2020 BezorgDirect. All rights reserved.
//

import Foundation
import SnapKit
import MTSlideToOpen

class ReturnToWarehouse: UIViewController, MTSlideToOpenDelegate {
    public var DELIVERY: DeliveryModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
        setupStyling()
    }
    
    private func initSetup() {
        // Styling navigation
        self.navigationItem.title = NSLocalizedString("cancel_delivery_title", comment: "")
        self.navigationController?.navigationBar.barTintColor = .systemYellow
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.view.backgroundColor = .black
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationItem.hidesBackButton = true
    }
    
    private func setupStyling() {
        guard let delivery = DELIVERY else { return }
        // layout styling and constraints
        let foccusedView = ReusableComponents.focussedView()
        self.view.addSubview(foccusedView)
        foccusedView.snp.makeConstraints({ make in
            make.top.equalTo(self.view.snp.top).offset(150)
            make.left.equalTo(self.view.snp.left).offset(10)
            make.right.equalTo(self.view.snp.right).offset(-10)
        })
        
        let labelTitle = ReusableComponents.label(text: NSLocalizedString("cancel_return_message1", comment: ""), color: 0)
        foccusedView.addSubview(labelTitle)
        labelTitle.snp.makeConstraints({ make in
            make.top.equalTo(foccusedView.snp.top).offset(20)
            make.right.equalTo(foccusedView.snp.right).offset(-20)
            make.left.equalTo(foccusedView.snp.left).offset(20)
        })
        
        let svAdres = ReusableComponents.adressSV(adres: delivery.warehouse.address)
        foccusedView.addSubview(svAdres)
        svAdres.snp.makeConstraints({ make in
            make.top.equalTo(labelTitle.snp.bottom).offset(40)
            make.left.equalTo(foccusedView.snp.left).offset(20)
            make.right.equalTo(foccusedView.snp.right).offset(-20)
        })
        
        let buttonMaps = ReusableComponents.Button(text: "Open route in maps", icon: "iconMaps", color: .systemYellow)
        buttonMaps.contentEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        buttonMaps.addTarget(self, action: #selector(openRoute), for: .touchUpInside)
        foccusedView.addSubview(buttonMaps)
        buttonMaps.snp.makeConstraints({ make in
            make.top.equalTo(svAdres.snp.bottom).offset(40)
            make.left.equalTo(foccusedView.snp.left).offset(20)
            make.right.equalTo(foccusedView.snp.right).offset(-20)
        })
        
        let slide = ReusableComponents.slider(text: NSLocalizedString("cancel_return_messageSlider", comment: ""))
        slide.delegate = self
        foccusedView.addSubview(slide)
        slide.snp.makeConstraints({ make in
            make.top.equalTo(buttonMaps.snp.bottom).offset(80)
            make.left.equalTo(foccusedView.snp.left).offset(20)
            make.right.equalTo(foccusedView.snp.right).offset(-20)
            make.bottom.equalTo(foccusedView.snp.bottom).offset(-40)
        })
    }
    
    @objc func openRoute() {
        guard let delivery = DELIVERY else { return }
        let long = delivery.warehouse.longitude
        let lat = delivery.warehouse.latitude
        let vehicle = "bicycle"
        let url = URL(string: "comgooglemaps://?saddr=&daddr=\(lat),\(long)&directionsmode=\(vehicle)")!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    func mtSlideToOpenDelegateDidFinish(_ sender: MTSlideToOpenView) {
        self.dismiss(animated: true, completion: nil)
    }
}
