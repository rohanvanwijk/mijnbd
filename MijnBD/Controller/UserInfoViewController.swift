//
//  UserInfoViewController.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 08/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit
import SwiftKeychainWrapper
import Foundation

class UserInfoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var localizedNameLabel: UILabel!
    @IBOutlet weak var localizedAddressLabel: UILabel!
    @IBOutlet weak var localizedBirthDateLabel: UILabel!
    @IBOutlet weak var localizedLicencesLabel: UILabel!
    @IBOutlet weak var localizedEmailLabel: UILabel!
    @IBOutlet weak var localizedPhoneNumberLabel: UILabel!
    
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var birthDateLabel: UILabel!
    @IBOutlet weak var licencesLabel: UILabel!
    @IBOutlet weak var buttonUpload: UIButton!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    var bezorger: DelivererModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let center: NotificationCenter = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        hideKeyboard()
        initSetup()
        setUserImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUserImage()
        getPerson()
        setData()
    }
    
    private func initSetup() {
        let saveButton = UIBarButtonItem(title: NSLocalizedString("userinfo_save_button_text", comment: ""), style: .plain, target: self, action: #selector(saveChanges(_:)))
        saveButton.tintColor = .black
        self.navigationItem.rightBarButtonItem = saveButton
        
        // Styling
        self.navigationItem.title = NSLocalizedString("userinfo_title", comment: "")
        self.navigationController?.navigationBar.barTintColor = .systemYellow
        self.navigationController?.navigationBar.tintColor = .black
        self.view.backgroundColor = .black
        nameLabel.layer.masksToBounds = true
        nameLabel.layer.cornerRadius = 10
        nameLabel.layer.borderWidth = 2
        nameLabel.layer.borderColor = UIColor.black.cgColor
        addressLabel.layer.masksToBounds = true
        addressLabel.layer.cornerRadius = 10
        addressLabel.layer.borderWidth = 2
        addressLabel.layer.borderColor = UIColor.black.cgColor
        birthDateLabel.layer.masksToBounds = true
        birthDateLabel.layer.cornerRadius = 10
        birthDateLabel.layer.borderWidth = 2
        birthDateLabel.layer.borderColor = UIColor.black.cgColor
        licencesLabel.layer.masksToBounds = true
        licencesLabel.layer.cornerRadius = 10
        licencesLabel.layer.borderWidth = 2
        licencesLabel.layer.borderColor = UIColor.black.cgColor
        profilePicture.layer.masksToBounds = true
        profilePicture.layer.cornerRadius = 10
        profilePicture.layer.borderWidth = 2
        profilePicture.layer.borderColor = UIColor.black.cgColor
        
        //setup labels
        localizedNameLabel.text = NSLocalizedString("userinfo_name_label", comment: "")
        localizedAddressLabel.text = NSLocalizedString("userinfo_address_label", comment: "")
        localizedBirthDateLabel.text = NSLocalizedString("userinfo_birth_date_label", comment: "")
        localizedLicencesLabel.text = NSLocalizedString("userinfo_licences_label", comment: "")
        localizedEmailLabel.text = NSLocalizedString("userinfo_email_label", comment: "")
        localizedPhoneNumberLabel.text = NSLocalizedString("userinfo_phone_number_label", comment: "")
        buttonUpload.setTitle(NSLocalizedString("userinfo_upload_image", comment: ""), for: .normal)
    }
    
    private func setUserImage() {
        if let imageData = KeychainWrapper.standard.data(forKey: "userImage") {
            let userImage = UIImage(data: imageData)
            profilePicture.image = userImage
            profilePicture.contentMode = .scaleAspectFill
        }
    }
    
    @IBAction func doUpload(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let userImage = info[.originalImage] as! UIImage
        profilePicture.image = userImage
        let imageData = userImage.pngData()
        KeychainWrapper.standard.set(imageData!, forKey: "userImage")
        profilePicture.contentMode = .scaleAspectFill
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    private func getPerson() {
        guard let data = KeychainWrapper.standard.data(forKey: "person") else { return }
        let decoder = JSONDecoder()
        do {
            let deliverer  = try
            decoder.decode(DelivererModel.self, from: data)
            self.bezorger = deliverer
        } catch let error {
            print(error)
        }
    }
    
    private func setData() {
        guard let p = bezorger else { return }
        
        //nameLabel.text = deliverer.emailAddress
        nameLabel.text = "  \(p.firstName)"
        let birthDateArray = p.dateOfBirth.components(separatedBy: "T")
        let birthDateArrayComponents = birthDateArray[0].components(separatedBy: "-")
        
        
        birthDateLabel.text = "  \(birthDateArrayComponents[2])-\(birthDateArrayComponents[1])-\(birthDateArrayComponents[0])"
        
        addressLabel.text = "  \(p.home.address)"
        licencesLabel.text = "  A;B"
        emailTextField.text = p.emailAddress
        phoneNumberTextField.text = p.phoneNumber
    }
    
    func alert(title: String, message: String) {
           let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
           alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: "ok"), style: .default, handler: nil))
           self.present(alert, animated: true, completion: nil)
       }
    
    private func save() {
        guard let p = bezorger else { return }
        let email = emailTextField.text ?? p.emailAddress
        let phone = phoneNumberTextField.text ?? p.phoneNumber
        
        let updatedPerson = DelivererModel(phoneNumber: phone, homeID: p.homeID, dateOfBirth: p.dateOfBirth, range: p.range, vehicle: p.vehicle, fare: p.fare, totalEarnings: p.totalEarnings, home: p.home, id: p.id, firstName: p.firstName, lastName: p.lastName, emailAddress: email, token: p.token)
        
        ServiceManager.updateDeliverer(person: updatedPerson, token: updatedPerson.token).responseData(completionHandler: { response in
            guard let responseData = response.data else { return }
            guard let httpcode = response.response?.statusCode else { return }
            
            print(httpcode)
            if httpcode == 200 {
                KeychainWrapper.standard.set(responseData, forKey: "person")
                self.navigationController?.popViewController(animated: true)
            } else {
                self.alert(title: NSLocalizedString("notification_saving_error_title", comment: ""), message: "\(NSLocalizedString("notification_saving_error_connection_error", comment: "")): \(httpcode)")
            }
            
        })
    }
    
    @IBAction func saveChanges(_ sender: UIButton) {
        if emailTextField.text != ""  && phoneNumberTextField.text != "" {
            save()
        } else {
            alert(title: NSLocalizedString("notification_saving_error_title", comment: ""), message: NSLocalizedString("notification_saving_error_empty_fields", comment: ""))
        }
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        })
    }
    
    func hideKeyboard() {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
    }
}
