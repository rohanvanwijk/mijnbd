//
//  DeliveryViewController.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 16/11/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import UIKit
import SnapKit
import SwiftKeychainWrapper
import CoreLocation
import UserNotifications

class DeliveryViewController: UIViewController {
    let waitLabel: UILabel = {
        let label = UILabel()
        label.text = NSLocalizedString("delivery_empty", comment: "")
        label.textColor = .white
        return label
    }()
    // Variables
    var model: DeliveryModel?
    var notification: NotificationModel?
    var hasNotification = false
    let locMan = CLLocationManager()
    var currentLocation: CLLocation!
    var notificationView: UIView?
    var timer: Timer!
    let notificationCenter = UNUserNotificationCenter.current()
    var backgroundTask: UIBackgroundTaskIdentifier = .invalid
    var spinner: UIActivityIndicatorView?
    
    // Outlets
    @IBOutlet weak var labelEarnings: UILabel!
    @IBOutlet weak var labelVehicle: UILabel!
    @IBOutlet weak var labelTravelTime: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var labelAddres: UILabel!
    @IBOutlet weak var imageVehicle: UIImageView!
    @IBOutlet weak var labelCustomerAddress: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ask for gps
        locMan.requestWhenInUseAuthorization()
        initSetup()
        // check delivery status and serve deliveryflow viewcontroller
        checkOpenDelivery()
        registerBackgroundTask()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateDelivery()
        getCurrentLocation()
        StartTimer()
    }
    
    private func initSetup() {
        // Styling
        self.navigationItem.title = NSLocalizedString("delivery_title", comment: "")
        self.navigationController?.navigationBar.barTintColor = .systemYellow
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        
        notificationView = UINib(nibName: "IncomingNotification", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
        setupRefreshButton()
        setupWaitLabel()
    }
    
    private func setupWaitLabel() {
        // waiting label
        self.view.addSubview(waitLabel)
        waitLabel.snp.makeConstraints({ make in
            make.center.equalTo(self.view)
        })
        
        spinner = ReusableComponents.spinner()
        guard let s = spinner else { return }
        self.view.addSubview(s)
        s.snp.makeConstraints({ make in
            make.left.equalTo(self.view.snp.left).offset(50)
            make.right.equalTo(self.view.snp.right).offset(-50)
            make.top.equalTo(waitLabel.snp.bottom).offset(10)
        })
    }
    
    private func setupNotification() {
        notificationCenter.requestAuthorization(options: [.alert, .sound, .badge], completionHandler: { (granted, error) in
            print(error)
        })
    }
    
    private func sendNotification() {
        guard let dModel = notification?.delivery else { return }
        guard let nModel = notification else { return }
        let content = UNMutableNotificationContent()
        content.title = NSLocalizedString("notification_title", comment: "")
        content.body = "\(NSLocalizedString("notification_address", comment: "")): \(dModel.customer.address), \(NSLocalizedString("notification_fare", comment: "")): \(String(format: "€ %.2f", dModel.price))"
        
        let date = Date().addingTimeInterval(2)
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        let request = UNNotificationRequest(identifier: nModel.id, content: content, trigger: trigger)
        notificationCenter.add(request, withCompletionHandler: { error in
            print(error)
        })
    }
    
    func updateDelivery() {
        guard let s = spinner else { return }
        s.startAnimating()
        guard let token = KeychainWrapper.standard.string(forKey: "token") else {return}
        ServiceManager.getNotification(token: token).responseData(completionHandler: { response in
            guard let jsonData = response.data else {return}
            let httpcode = response.response?.statusCode
            
            if httpcode == 204 {
                guard let n = self.notificationView else { return }
                n.removeFromSuperview()
                self.setupWaitLabel()
                self.hasNotification = false
                self.setupRefreshButton()
                self.waitLabel.text = NSLocalizedString("waitlabel_text", comment: "")
                s.stopAnimating()
            } else if httpcode == 200 {
                let decoder = JSONDecoder()
                do {
                    let notification = try
                        decoder.decode(NotificationModel.self, from: jsonData)
                    //create notification view
                    self.hasNotification = true
                    self.setupRefreshButton()
                    self.notification = notification
                    self.CreateNotificationViewNib()
                    self.getDirections()
                    s.stopAnimating()
                } catch let error {
                    print(error)
                }
            } else {
                self.setupWaitLabel()
                self.hasNotification = false
                self.setupRefreshButton()
                self.waitLabel.text = NSLocalizedString("waitlabel_error", comment: "")
                s.stopAnimating()
            }
        })
    }
    
    private func getCurrentLocation() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways {
            currentLocation = locMan.location
            print("long: \(currentLocation.coordinate.longitude), lat: \(currentLocation.coordinate.latitude)")
        } else {
            alert(title: NSLocalizedString("locationAllow_title", comment: ""), message: NSLocalizedString("locationAllow_message", comment: ""))
        }
    }
    
    func alert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: "ok"), style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func setupRefreshButton() {
        if !hasNotification {
            let refreshButton = UIBarButtonItem(title: NSLocalizedString("refresh_button", comment: ""), style: .plain, target: self, action: #selector(refresh))
            refreshButton.tintColor = .black
            self.navigationItem.rightBarButtonItem = refreshButton
        } else {
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    @objc func refresh() {
        updateDelivery()
    }
    
    // Get directions from Google and fil View
    private func getDirections() {
        print("DIRECTIONSSSS")
        guard let not = notification else { return }
        var travelMode = "driving"
        if not.delivery.vehicle == 1 { travelMode = "bicycling" }
        ServiceManager.getDirections(travelMode: travelMode, originLat: not.delivery.warehouse.latitude, originLong: not.delivery.warehouse.longitude, desLat: not.delivery.customer.latitude, desLong: not.delivery.customer.longitude).responseData(completionHandler: { response in
            guard let jsonData = response.data else { return }
            guard let httpCode = response.response?.statusCode else { return }
            let dueDate = self.getDueDateString(dueDateString: not.delivery.dueDate)
            
            if httpCode == 200 {
                do {
                    let decoder = JSONDecoder()
                    let result = try
                        decoder.decode(DirectionsModel.self, from: jsonData)
                    let travelTime = result.routes[0].legs[0].duration.text
                    let travelDistance = result.routes[0].legs[0].distance.text
                    self.labelAddres.text = travelDistance
                    self.labelTravelTime.text = travelTime + " Geschatte reistijd \n\(dueDate) Uiterlijke levertijd"
                } catch {
                    print(error)
                }
            }
        })
    }
    
    public func StartTimer() {
        self.timer = Timer.scheduledTimer(withTimeInterval: 50.0, repeats: true, block: { timer in
            if self.hasNotification {
                // check time left
                // notifify to user
                // refuse notifaction after 10 min.
                // set hasNotification to false when refuse
                let now = timer.fireDate
                let nowString = now.toString(dateFormat: "yyyy-MM-dd HH:mm:ss")
                let f = DateFormatter()
                f.timeZone = TimeZone(abbreviation: "UTC")
                f.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let nowNewDate = f.date(from: nowString)!

                let expiredString = self.notification!.expiredAt

                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSSSSS"
                formatter.timeZone = TimeZone(abbreviation: "UTC")
                formatter.locale = Locale(identifier: "en_US_POSIX")
                let dateExpired = formatter.date(from: expiredString)!

                print("\(dateExpired) - \(nowNewDate)")
                // check if notification is expired
                if nowNewDate >= dateExpired {
                    self.refuseNotification()
                    self.hasNotification = false
                }

                let exTimeInterval = dateExpired.timeIntervalSince1970
                let exInt = Int(exTimeInterval)

                let start = exInt - 600

                let nowTimeInterval = nowNewDate.timeIntervalSince1970
                let nowInt = Int(nowTimeInterval)

                let length = Float(exInt) - Float(start)
                let deel = Float(nowInt) - Float(start)
                let percentage: Float = deel/length
                print("-> \(percentage/10)")
                self.progressBar.setProgress(percentage, animated: true)
            } else {
                // check for notifications
                self.updateDelivery()
            }
        })
    }
    
    private func StopTimer() {
        self.timer.invalidate()
    }
    
    private func CreateNotificationViewNib() {
        guard let s = spinner else { return }
        s.removeFromSuperview()
        createNotificationMessage()
        sendNotification()
        waitLabel.removeFromSuperview()
        guard let notView = notificationView else {return}
        
        if !notView.isDescendant(of: self.view) {
            self.view.addSubview(notView)
            notView.snp.makeConstraints({ make in
                make.top.equalTo(self.view.snp.top)
                make.left.equalTo(self.view.snp.left)
                make.right.equalTo(self.view.snp.right)
                make.bottom.equalTo(self.view.snp.bottom)
            })
        }
    }
    
    @IBAction func buttonRefused(_ sender: Any) {
        refuseNotification()
    }
    
    private func refuseNotification() {
        guard let notView = notificationView else {return}
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        guard let not = notification else { return }
        notView.removeFromSuperview()
        setupWaitLabel()
        
        ServiceManager.respondNotification(token: token, accepted: false, notificationId: not.id).responseJSON(completionHandler: { response in
            if response.response?.statusCode == 200 {
                print("Refused: \(not.id)")
                self.updateDelivery()
            }
        })
    }
    
    @IBAction func buttonAccepted(_ sender: Any) {
        acceptNotification()
    }
    
    private func acceptNotification() {
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        guard let not = notification else { return }
        
        ServiceManager.respondNotification(token: token, accepted: true, notificationId: not.id).responseJSON(completionHandler: { response in
            if response.response?.statusCode == 200 {
                print("Accepted deliveryId: \(not.id)")
                self.startDeliveryFlow()
                self.hasNotification = false
                self.StopTimer()
                self.notificationView!.removeFromSuperview()
            }
        })
    }
    
    private func getDelivery() {
        guard let not = notification else { return }
        ServiceManager.getDelivery(deliveryId: not.delivery.id).responseData(completionHandler: { response in
            guard let jsonData = response.data else {return}
            let httpCode = response.response?.statusCode
            
            if httpCode == 200 {
                do {
                    let decoder = JSONDecoder()
                    let result = try
                        decoder.decode(DeliveryModel.self, from: jsonData)
                    self.model = result
                    self.createNotificationMessage()
                    self.sendNotification()
                } catch {
                    print(error)
                }
            }
        })
    }
    
    private func checkOpenDelivery() {
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        ServiceManager.checkDelivery(token: token).responseData(completionHandler: { response in
            guard let jsonData = response.data else {return}
            let httpCode = response.response?.statusCode
            
            if httpCode == 200 {
                do {
                    let decoder = JSONDecoder()
                    let result = try
                        decoder.decode(DeliveryModel.self, from: jsonData)
                    self.model = result
                    self.hasNotification = true
                    self.startDeliveryFlow()
                    self.StopTimer()
                } catch {
                    print(error)
                }
            }
        })
    }
    
    private func startDeliveryFlow() {
        let nav = UINavigationController()
        let vc = ToWarehouseViewController()
        if notification?.delivery != nil {
            vc.DELIVERY = notification?.delivery
        } else {
            vc.DELIVERY = model
        }
        vc.NOTIFICATION = notification
        nav.viewControllers = [vc]
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
    }
    
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self!.updateDelivery()
            self?.endBackgroundTask()
        }
        assert(backgroundTask != .invalid)
    }
    
    func endBackgroundTask() {
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = .invalid
    }
    
    private func createNotificationMessage() {
        guard let delivery = notification?.delivery else { return }
        
        let dueDate = getDueDateString(dueDateString: delivery.dueDate)
        
        let price = Double(delivery.price)
        labelEarnings?.text = "\(price.euroString) \(NSLocalizedString("notification_fare", comment: ""))"
        
        labelVehicle?.text = "\(mapVehicle(_vehicle: delivery.vehicle)) \(NSLocalizedString("notification_vehicle_extratext", comment: ""))"
        labelTravelTime?.text = "\(dueDate) \(NSLocalizedString("notification_deadline", comment: ""))"
        labelAddres.text = delivery.customer.address
        labelCustomerAddress.text = delivery.customer.address
        
        var image = UIImage(named: "ic_bike_yellow")
        if delivery.vehicle == 2 || delivery.vehicle == 3 {
            image = UIImage(named: "iconMotor")
        } else if delivery.vehicle == 4 {
            image = UIImage(named: "ic_car_yellow")
        }
        imageVehicle.image = image
    }
    
    private func getDueDateString(dueDateString: String) -> String {
        let dFormatter = DateFormatter()
        dFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss"
        dFormatter.timeZone = TimeZone(abbreviation: "UTC")
        if let dueDate = dFormatter.date(from: dueDateString) {
            var calendar = Calendar.current
            calendar.timeZone = TimeZone(abbreviation: "UTC")!
            let time=calendar.dateComponents([.hour, .minute], from: dueDate)
            let dueDateString = String(format: "%02d:%02d", time.hour!, time.minute!)
            return dueDateString
        }
        return ""
    }
    
    private func mapVehicle(_vehicle: Int) -> String {
        switch _vehicle {
        case 1:
            return NSLocalizedString("bicycle", comment: "")
        case 2:
            return NSLocalizedString("scooter", comment: "")
        case 3:
            return NSLocalizedString("motorcycle", comment: "")
        default:
            return NSLocalizedString("car", comment: "")
        }
    }
}
extension Double {
    var euroString:String {
        return String(format: "%.2f", self)
    }
}
extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
}
