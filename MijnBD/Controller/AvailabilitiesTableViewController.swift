//
//  AvailabilitiesTableViewController.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 08/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit
import SwiftKeychainWrapper

class AvailabilitiesTableViewController: UITableViewController {
    var list: [AvailabilityModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
        getData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getData()
    }
    
    private func initSetup() {
        // Styling
        self.navigationItem.title = NSLocalizedString("availability_title", comment: "")
        self.navigationController?.navigationBar.barTintColor = .systemYellow
        self.tableView.backgroundColor = .black
        self.navigationController?.navigationBar.tintColor = .black
        
        // Register History cell
        let cellNib = UINib(nibName: "AvailabilityCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: "avCell")
        
        // Add button
        let addButton = UIButton()
        addButton.frame = CGRect(x: 10, y: 20, width: 50, height: 50)
        addButton.setTitle("+", for: .normal)
        addButton.titleLabel?.font = .boldSystemFont(ofSize: 27)
        addButton.setTitleColor(.black, for: .normal)
        addButton.addTarget(self, action: #selector(addAvailability), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: addButton)
    }
    
    private func getData() {
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        ServiceManager.getAvailabilities(token: token).responseJSON(completionHandler: { response in
            guard let jsonData = response.data else {return}
            
            let decoder = JSONDecoder()
            do {
                let result = try
                    decoder.decode([AvailabilityModel].self, from: jsonData)
                self.list = result
                self.tableView.reloadData()
            } catch let error {
                print(error)
            }
        })
    }
    
    @objc func addAvailability() {
        let vc = AddAvailabilityViewController(nibName: "AddAvailabilityView", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func removeAvailability(index: Int) {
        guard let token = KeychainWrapper.standard.string(forKey: "token") else { return }
        let model = list[index]
        ServiceManager.removeAvailability(token: token, id: model.id).response(completionHandler: { response in
            guard let httpcode = response.response?.statusCode else { return }
            if httpcode == 200 {
                self.list.remove(at: index)
                self.tableView.reloadData()
            }
        })
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "avCell", for: indexPath) as! AvailabilityCell
        
        list.sort{
            if $0.date != $1.date {
                return $0.date < $1.date
            }
            else {
                return $0.startTime < $1.startTime
            }
        }
            
        
        let model = list[indexPath.row]
        
        let jsonFormatter = DateFormatter()
        jsonFormatter.timeZone = .current
        jsonFormatter.locale = Locale(identifier: "en_US_POSIX")
        jsonFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss"
        let jsonDate = jsonFormatter.date(from: model.date)!
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let date = formatter.string(from: jsonDate)
       
        let subStringStart = model.startTime.dropLast(3)
        let subStringEnd = model.endTime.dropLast(3)
        let newStartTime = String(subStringStart)
        let newEndTime = String(subStringEnd)
        
        cell.dateLabel.text = date
        cell.startTimeLabel.text = newStartTime
        cell.endTimeLabel.text = newEndTime
    
        return cell
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .default, title: "Verwijder", handler: { action, indexPath in
            self.removeAvailability(index: indexPath.row)
        })
        return [delete]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = list[indexPath.row]
        print(model)
        let vc = AddAvailabilityViewController(nibName: "AddAvailabilityView", bundle: nil)
        vc.isPut = true
        vc.date = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
}
