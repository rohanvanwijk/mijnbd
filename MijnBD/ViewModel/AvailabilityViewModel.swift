//
//  AvailabilityViewModel.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 26/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation

struct AvailabilityViewModel: Codable {
    var date, startTime, endTime: String

    enum CodingKeys: String, CodingKey {
        case date = "Date"
        case startTime = "StartTime"
        case endTime = "EndTime"
    }
}
