//
//  DelivererViewModel.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 13/11/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation

struct DelivererViewModel {
    let email: String
    
    init(deliverer: DelivererModel) {
        self.email  = deliverer.emailAddress
    }
}
