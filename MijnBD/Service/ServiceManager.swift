//
//  ServiceManager.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 15/11/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import Alamofire
import CoreLocation

final class ServiceManager {
    static let API_URL = "http://192.168.178.42:7071/api"
    
    static func login(email: String, password: String) -> DataRequest {
        let body: [String: String] = ["emailAddress": email, "password": password]
        return Alamofire.request(API_URL + "/login", method: .post, parameters: body, encoding: JSONEncoding.default)
    }
    
    static func logout(token: String) -> DataRequest {
        var request = URLRequest(url: URL(string: API_URL + "/logout")! as URL)
        request.httpMethod = "POST"
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        return Alamofire.request(request)
    }
    
    static func getAvailabilities(token: String) -> DataRequest {
        var request = URLRequest(url: URL(string: API_URL + "/availabilities")! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        return Alamofire.request(request)
    }
    
    static func addAvailability(token: String, availability: AvailabilityModel) -> DataRequest {
        var request = URLRequest(url: URL(string: API_URL + "/availabilities")! as URL)
        request.httpMethod = "POST";
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization");
        request.setValue("application/json", forHTTPHeaderField: "Content-Type");
        
        let encoder = JSONEncoder()
        let model = AvailabilityViewModel(date: availability.date, startTime: availability.startTime, endTime: availability.endTime)
        let list = [model]
        let data = try? encoder.encode(list)
        print(String(data: data!, encoding: .utf8)!)
        request.httpBody = data!
      
        return Alamofire.request(request)
    }
    
    static func removeAvailability(token: String, id: String) -> DataRequest {
        var request = URLRequest(url: URL(string: API_URL + "/availabilities/\(id)")! as URL);
        request.httpMethod = "DELETE";
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization");
        request.setValue("application/json", forHTTPHeaderField: "Content-Type");
        return Alamofire.request(request);
    }
    
    static func editAvailability(token: String, model: AvailabilityModel) -> DataRequest {
        var request = URLRequest(url: URL(string: API_URL + "/availabilities")! as URL);
        request.httpMethod = "PUT";
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization");
        request.setValue("application/json", forHTTPHeaderField: "Content-Type");
        let encoder = JSONEncoder()
        let list = [model]
        let data = try? encoder.encode(list)
        print(String(data: data!, encoding: .utf8)!)
        request.httpBody = data!
        return Alamofire.request(request);
    }
    
    static func getOrderHistory(token: String) -> DataRequest {
        var request = URLRequest(url: URL(string: API_URL + "/deliveries")! as URL);
        request.httpMethod = "GET";
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization");
        request.setValue("application/json", forHTTPHeaderField: "Content-Type");
        return Alamofire.request(request);
    }

    
    static func getNotification(token: String) -> DataRequest {
        var request = URLRequest(url: URL(string: API_URL + "/notifications")! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        return Alamofire.request(request)
    }
    
    static func respondNotification(token: String, accepted: Bool, notificationId: String) -> DataRequest {
        let json: [String: Bool] = ["accepted": accepted]
        let bodyData = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        var request = URLRequest(url: URL(string: API_URL + "/notifications/\(notificationId)")! as URL)
        request.httpMethod = "PATCH"
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = bodyData
        return Alamofire.request(request)
    }
    
    static func getDelivery(deliveryId: String) -> DataRequest {
        let adminToken = "GERG15TRBrgd24EFhgrvfTHG34RHtrntyj1n65yj2"
        let url = URL(string: API_URL + "/deliveries/\(deliveryId)")! as URL
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("Bearer \(adminToken)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        return Alamofire.request(request)
    }
    
    //update status of delivery 0=afgemeld, 1=aangekondigd, 2=bevestigd, 3=onderweg, 4=afgeleverd
    static func updateDelivery(deliveryId: String, status: Int, token: String, currentLocation: CLLocation) -> DataRequest {
        let json: [String: Any] = ["status": status, "latitude": currentLocation.coordinate.latitude, "longitude": currentLocation.coordinate.longitude, "warehouseDistance": 5.434543, "clientDistance": 5.23432432]
        let bodyData = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        var request = URLRequest(url: URL(string: API_URL + "/deliveries/\(deliveryId)/status")! as URL)
        request.httpMethod = "PATCH"
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = bodyData
        return Alamofire.request(request)
    }
    
    static func putMe(phoneNumber: String, homeId: String, dateOfBirth: String, range: Int, vehicle: Int, vehicleDisplayName: String, fare: Double, totalEarnings: Int, home: Home, id: String, emailAddress: String, token: String) -> DataRequest {
        let json: [String: Any] = ["phoneNumber": phoneNumber, "dateOfBirth": dateOfBirth, "range": range, "vehicle": vehicle, "vehicleDisplayName": vehicleDisplayName, "fare": fare, "totalEarnings": totalEarnings, "home": ["id": home.id], "id": id, "emailAddress": emailAddress]
        let bodyData = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        var request = URLRequest(url: URL(string: API_URL + "/me")! as URL)
        request.httpMethod = "PUT"
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = bodyData
        return Alamofire.request(request)
    }
    
    static func updateDeliverer(person: DelivererModel, token: String) -> DataRequest {
        let encoder = JSONEncoder()
        let data = try? encoder.encode(person)
        var request = URLRequest(url: URL(string: API_URL + "/me")! as URL)
        request.httpMethod = "PUT"
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = data!
        return Alamofire.request(request)
    }
    
    static func getUser(token: String) -> DataRequest {
        var request = URLRequest(url: URL(string: API_URL + "/me")! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json;charset=utf-8", forHTTPHeaderField: "Content-Type")
        return Alamofire.request(request)
    }
    
    static func checkDelivery(token: String) -> DataRequest {
        let url = URL(string: API_URL + "/deliveries/current")! as URL
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        return Alamofire.request(request)
    }
    
    static func getEarnings(token: String, timeframe: String) -> DataRequest {
        let url = URL(string: API_URL + "/me/earnings?timeframe=\(timeframe)")! as URL
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        return Alamofire.request(request)
    }
    
    static func getDirections(travelMode: String, originLat: Double, originLong: Double, desLat: Double, desLong: Double) -> DataRequest {
        let key = "AIzaSyDXcZ1kNGmIoqILUeTlSlW_Ljz_nSlPD18"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(originLat),\(originLong)&destination=\(desLat),\(desLong)&key=\(key)&travelmode=\(travelMode)"
        return Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
    }
}
