//
//  AppDelegate.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 12/11/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    let vc = DeliveryViewController()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // Background fetch
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
        return true
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        vc.updateDelivery()
        if vc.hasNotification {
            print("received background notification!")
            completionHandler(.newData)
        } else {
            completionHandler(.noData)
        }
        print("background task here.")
    }
}

