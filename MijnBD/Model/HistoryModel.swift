//
//  HistoryModel.swift
//  MijnBD
//
//  Created by Kevin Sweijen on 19/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation

// MARK: - HistoryModelElement
struct HistoryModel: Codable {
    let id, delivererID, customerPhoneNumber, dueDate: String
    let vehicle: Int
    let startedAtID: String
    let warehouseDistanceInKilometers: Double
    let warehouseETA, warehouseID, warehousePickUpAt: String
    let customerDistanceInKilometers: Double
    let customerETA, customerID, currentID, deliveredAt: String
    let price: Double
    let tip: Double?
    let paymentMethod, status: Int
    let warehouse: Customer
    let customer: Customer
    let current: Current?
    let createdAt: String?
    let acceptedAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case delivererID = "delivererId"
        case customerPhoneNumber, dueDate, vehicle
        case startedAtID = "startedAtId"
        case warehouseDistanceInKilometers, warehouseETA
        case warehouseID = "warehouseId"
        case warehousePickUpAt, customerDistanceInKilometers, customerETA
        case customerID = "customerId"
        case currentID = "currentId"
        case deliveredAt, price, tip, paymentMethod, status, warehouse, customer, current, acceptedAt, createdAt
    }
}
