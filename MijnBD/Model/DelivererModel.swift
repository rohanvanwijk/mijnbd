//
//  Deliverer.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 13/11/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.

import Foundation

// MARK: - DelivererModel
struct DelivererModel: Codable {
    let phoneNumber, homeID, dateOfBirth: String
    let range, vehicle: Int
    let fare: Double
    let totalEarnings: Double?
    let home: Home
    let id, firstName, lastName, emailAddress: String
    let token: String

    enum CodingKeys: String, CodingKey {
        case phoneNumber
        case homeID = "homeId"
        case dateOfBirth, range, vehicle, fare, totalEarnings, home, id, firstName, lastName, emailAddress, token
    }
}

// MARK: - Home
struct Home: Codable {
    let id: String
    let latitude, longitude: Double
    let address, postalCode, place: String
    let isWarehouse: Bool
}
