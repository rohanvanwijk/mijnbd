// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let notificationModel = try? newJSONDecoder().decode(NotificationModel.self, from: jsonData)

import Foundation

// MARK: - NotificationModel
struct NotificationOWModel: Codable {
    let id, delivererID, customerPhoneNumber, dueDate: String?
    let vehicle: Int?
    let startedAtID: String?
    let warehouseDistanceInKilometers: Double?
    let warehouseETA, wareHouseID, warehousePickUpAt: String?
    let customerDistanceInKilometers: Double?
    let customerETA, customerID, currentID, deliveredAt: String?
    let price, tip: Double?
    let paymentMethod: Int?
    let paymentMethodDisplayName: String?
    let status: Int?
    let statusDisplayName: String?
    let warehouse, customer, current: Current?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case delivererID = "DelivererId"
        case customerPhoneNumber = "CustomerPhoneNumber"
        case dueDate = "DueDate"
        case vehicle = "Vehicle"
        case startedAtID = "StartedAtId"
        case warehouseDistanceInKilometers = "WarehouseDistanceInKilometers"
        case warehouseETA = "WarehouseETA"
        case wareHouseID = "WareHouseId"
        case warehousePickUpAt = "WarehousePickUpAt"
        case customerDistanceInKilometers = "CustomerDistanceInKilometers"
        case customerETA = "CustomerETA"
        case customerID = "CustomerId"
        case currentID = "CurrentId"
        case deliveredAt = "DeliveredAt"
        case price = "Price"
        case tip = "Tip"
        case paymentMethod = "PaymentMethod"
        case paymentMethodDisplayName = "PaymentMethodDisplayName"
        case status = "Status"
        case statusDisplayName = "StatusDisplayName"
        case warehouse = "Warehouse"
        case customer = "Customer"
        case current = "Current"
    }
}

// MARK: - Current
struct Current: Codable {
    let id: String?
    let latitude, longitude: Double?
    var address, postalCode, place: String?
    let isWareHouse: Bool?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case latitude = "Latitude"
        case longitude = "Longitude"
        case address = "Address"
        case postalCode = "PostalCode"
        case place = "Place"
        case isWareHouse = "IsWareHouse"
    }
}
