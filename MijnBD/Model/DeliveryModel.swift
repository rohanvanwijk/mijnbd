//
//  DeliveryModel.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 09/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation

// MARK: - DeliveryModel
struct DeliveryModel: Codable {
    let id: String
    let delivererID: String?
    let customerPhoneNumber, dueDate: String
    let vehicle: Int
    let startedAtID, warehouseETA: String?
    let warehouseDistanceInKilometers: Double?
    let warehouseID: String
    let warehousePickUpAt, customerETA: String?
    let customerDistanceInKilometers: Double?
    let customerID: String
    let currentID, deliveredAt: String?
    let price: Double
    let tip: Double?
    let paymentMethod, status: Int
    let warehouse, customer: Customer
    let current: Current?

    enum CodingKeys: String, CodingKey {
        case id
        case delivererID = "delivererId"
        case customerPhoneNumber, dueDate, vehicle
        case startedAtID = "startedAtId"
        case warehouseDistanceInKilometers, warehouseETA
        case warehouseID = "warehouseId"
        case warehousePickUpAt, customerDistanceInKilometers, customerETA
        case customerID = "customerId"
        case currentID = "currentId"
        case deliveredAt, price, tip, paymentMethod, status, warehouse, customer, current
    }
}

// MARK: - Customer
struct Customer: Codable {
    let id: String
    let latitude, longitude: Double
    let address, postalCode, place: String
    let isWarehouse: Bool?
}
