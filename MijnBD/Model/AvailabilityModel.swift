//
//  AvailabilityModel.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 06/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation

// MARK: - DelivererModel
struct AvailabilityModel: Codable {
    var id, delivererID, date, startTime: String
    var endTime: String

    enum CodingKeys: String, CodingKey {
        case id
        case delivererID = "delivererId"
        case date
        case startTime
        case endTime
    }
}
