//
//  NotificationModel.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 08/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.

// MARK: - NotificationModel
struct NotificationModel: Codable {
    let id, delivererID, expiredAt: String
    let delivery: DeliveryModel

    enum CodingKeys: String, CodingKey {
        case id
        case delivererID = "delivererId"
        case expiredAt, delivery
    }
}
