//
//  AvaillabilityCell.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 08/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit

class AvailabilityCell: UITableViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        dateLabel.text = nil
        startTimeLabel.text = nil
        endTimeLabel.text = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let inset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        contentView.frame = contentView.frame.inset(by: inset)
    }
}
