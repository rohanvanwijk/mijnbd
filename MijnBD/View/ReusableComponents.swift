//
//  ReusableComponents.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 19/12/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit
import MTSlideToOpen
import SnapKit

final class ReusableComponents {
    static func Button(text: String, icon: String, color: UIColor) -> UIButton {
        var image = UIImage(named: icon)
        image = image?.resize(maxWidthHeight: 25.0)
       
        
        let button = UIButton()
        button.layer.cornerRadius = 10
        button.tintColor = .black
        button.backgroundColor = color
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 25)
        button.setImage(image, for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.setTitle(text, for: .normal)
        return button
    }
    
    static func focussedView() -> UIView {
        let view = UIView()
        view.backgroundColor = .darkGray
        view.layer.cornerRadius = 20
        return view
    }
    
    static func spinner() -> UIActivityIndicatorView {
        let AV = UIActivityIndicatorView()
        AV.hidesWhenStopped = true
        AV.style = .whiteLarge
        return AV
    }
    
    static func label(text: String, color: Int) -> UILabel {
        var colorText: UIColor = .white
        if color == 1 {
            colorText = .systemYellow
        }
        let label = UILabel()
        label.numberOfLines = 0
        label.text = text
        label.font = label.font.withSize(20)
        label.textColor = colorText
        return label
    }
    
    static func image(icon: String) -> UIImageView {
        let image = UIImage(named: icon)
        let imageView = UIImageView(image: image)
        return imageView
    }
    
    static func heading1(text: String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.font = label.font.withSize(30)
        label.textColor = .white
        label.numberOfLines = 0
        return label
    }
    
    static func adressSV(adres: String) -> UIStackView {
        let label = self.label(text: "Adres", color: 1)
        let labelAdres = self.heading1(text: adres)
        let sv = UIStackView(arrangedSubviews: [label, labelAdres])
        sv.axis = .vertical
        sv.spacing = 10
        return sv
    }
    
    static func slider(text: String) -> MTSlideToOpenView {
        var image = UIImage(named: "iconTick")
        image = image?.resize(maxWidthHeight: 65)
       
        let slide = MTSlideToOpenView(frame: CGRect(x: 0, y: 0, width: 300, height: 70))
        slide.sliderViewTopDistance = 6
        slide.sliderCornerRadius = 40
        slide.labelText = text
        slide.sliderBackgroundColor = .white
        slide.textColor = .black
        slide.slidingColor = .systemYellow
        slide.tintColor = .systemYellow
        slide.thumbnailColor = .systemYellow
        slide.thumnailImageView.image = image
        return slide
    }
    
    static func alertMessage(text: String, icon: String) -> UIButton {
        let button = self.Button(text: text, icon: icon, color: .darkGray)
        button.setTitleColor(.white, for: .normal)
        return button
    }
}
extension UIImage {

    func resize(maxWidthHeight : Double)-> UIImage? {

        let actualHeight = Double(size.height)
        let actualWidth = Double(size.width)
        var maxWidth = 0.0
        var maxHeight = 0.0

        if actualWidth > actualHeight {
            maxWidth = maxWidthHeight
            let per = (100.0 * maxWidthHeight / actualWidth)
            maxHeight = (actualHeight * per) / 100.0
        }else{
            maxHeight = maxWidthHeight
            let per = (100.0 * maxWidthHeight / actualHeight)
            maxWidth = (actualWidth * per) / 100.0
        }

        let hasAlpha = true
        let scale: CGFloat = 0.0

        UIGraphicsBeginImageContextWithOptions(CGSize(width: maxWidth, height: maxHeight), !hasAlpha, scale)
        self.draw(in: CGRect(origin: .zero, size: CGSize(width: maxWidth, height: maxHeight)))

        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage
    }
}
