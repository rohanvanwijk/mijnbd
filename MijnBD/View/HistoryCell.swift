//
//  HistoryCell.swift
//  MijnBD
//
//  Created by Rohan van Wijk on 17/11/2019.
//  Copyright © 2019 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit

class HistoryCell: UITableViewCell {
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var iconCheck: UIImageView!
    @IBOutlet weak var iconVehicle: UIImageView!
    
    @IBOutlet weak var backView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupStyling()
    }
    
    private func setupStyling() {
        // Adres label
        addressLabel.textColor = .systemYellow
        addressLabel.font = .boldSystemFont(ofSize: 16)
        
        timeLabel.textColor = .white
        durationLabel.textColor = .white
        priceLabel.textColor = .white
        distanceLabel.textColor = .white
        
        // Cell
        self.backView.layer.cornerRadius = 10
        self.backView.backgroundColor = .darkGray
               
        let testImage = UIImage(named: "iconUser")
        iconCheck.image = testImage
        iconVehicle.image = testImage
        addressLabel.text = "Zuiderdiep 24, Haarlem"
        timeLabel.text = "17:02 - 18:11"
        durationLabel.text = "22 min."
        distanceLabel.text = "5 km."
        priceLabel.text = "23,00"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let inset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        contentView.frame = contentView.frame.inset(by: inset)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconCheck.image = nil
        iconVehicle.image = nil
        addressLabel.text = nil
        timeLabel.text = nil
        durationLabel.text = nil
        distanceLabel.text = nil
        priceLabel.text = nil
    }
}
