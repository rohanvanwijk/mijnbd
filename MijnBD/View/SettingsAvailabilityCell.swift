//
//  SettingsAvailabilityCell.swift
//  MijnBD
//
//  Created by Wessel Kok on 06/01/2020.
//  Copyright © 2020 BezorgDirect. All rights reserved.
//

import Foundation
import UIKit

class SettingsAvailabilityCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        dateLabel.text = nil
        startTimeLabel.text = nil
        endTimeLabel.text = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame
    }
}
