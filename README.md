# Bezorg Direct MijnBD

An iOS app for [Bezorg Direct](https://bezorg.direct/).

## How to get this project up and running

- Clone this repository (`git clone`)
- Install pods in root of project (`pod install`)

## Used frameworks

- UIKit
- CoreLocation
- UserNotifications
- SnapKit
- Alamofire
- SwiftKeychainWrapper
- MTSlideToOpen

_Information Technology, [InHolland](https://www.inholland.nl/inhollandcom/) university of applied sciences, November 2019_
